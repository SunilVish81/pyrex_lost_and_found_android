
import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Image, ScrollView, TouchableOpacity, Keyboard, Button, Platform } from 'react-native';
import { Dropdown } from 'react-native-element-dropdown';
import AntDesign from 'react-native-vector-icons/AntDesign';
import colors from '../../../components/colors';
import { TextInput } from 'react-native-element-textinput';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import Ionicons from 'react-native-vector-icons/Ionicons'; // Use appropriate icon library
import { API_BASE_URL } from '../../../ApiBaseSetUp';
import { Loader } from '../../../components/Loader';
import Entypo
    from 'react-native-vector-icons/Entypo';
import moment from 'moment';
import Toast from 'react-native-simple-toast';



const HandOverDetails = (props) => {
    const [showLoader, setShowLoader] = useState(true);
    const [itemliststore, setItemliststore] = useState([]);
    const [imgarr, setImgarr] = useState([]);
    const [status, setStatus] = useState("");


    const [itemname, setItemname] = useState("");
    const [catnam, setCatnam] = useState("");
    const [airlinenam, setAirlinenam] = useState("");
    const [itemstatus, setItemstatus] = useState("");
    const [flightnum, setFlightnum] = useState("");
    const [seatno, setSeatno] = useState("");
    const [standno, setStandNo] = useState("");
    const [des, setDes] = useState("");
    const [remark, setRemark] = useState("");
    const [passengername, setPassengername] = useState("");
    const [passengeremail, setPassengeremail] = useState("");
    const [passengerphone, setPassengerphone] = useState("");
    const [handoverat, setHandoverat] = useState("");
    const [handoverbyname, setHandoverbyname] = useState("");
    const [handoverto, setHandoverto] = useState("");
    const [signimgs, setSignimgs] = useState("");
    const [dateupdate, setDateupdate] = useState("");
    const [handoverdate, setHandoverdate] = useState("");

    const [isModalVisible, setModalVisible] = useState(false);
    const [selectedImageId, setSelectedImageId] = useState(null);
    const [baseimg, setBaseImg] = useState("");
    const [uploadedby, setUploadedby] = useState("");


    const toggleModal = (index) => {
        setModalVisible(!isModalVisible);
        setSelectedImageId(index);


    };

    React.useEffect(() => {
        const focusHandler = props.navigation.addListener('focus', () => {
            detailsFetch()
        });
        return focusHandler;
    }, [props]);

    const detailsFetch = async () => {
        setShowLoader(true)

        const Token = await AsyncStorage.getItem('token')
        await fetch(API_BASE_URL + 'user/get/handover/item/details', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${Token}`,

            },
            body: JSON.stringify({
                item_id: props?.route?.params?.id

            }),

        })
            .then(response => response.json())
            .then(res => {
                setBaseImg(res?.image_base_url)
                setImgarr(res?.data)
                setShowLoader(false)
                setStatus(res?.data?.items?.item_status)
                setItemname(res?.data?.items?.item_name)
                setCatnam(res?.data?.items?.category_name)
                setAirlinenam(res?.data?.items?.airline_name)
                setItemstatus(res?.data?.items?.item_status)
                setFlightnum(res?.data?.items?.flight_no)
                setSeatno(res?.data?.items?.seat_no)
                setStandNo(res?.data?.items?.stand_no)
                setDes(res?.data?.items?.item_description)
                setRemark(res?.data?.items?.remark)
                setPassengername(res?.data?.items?.passenger_name
                )
                setPassengeremail(res?.data?.items?.passenger_email)
                setPassengerphone(res?.data?.items?.passenger_phone)
                setHandoverat(res?.data?.items?.handover_at)
                setHandoverbyname(res?.data?.items?.handover_by_name
                )
                setHandoverto(res?.data?.items?.handover_to)
                setSignimgs(res?.data?.items?.signature)
                setDateupdate(res?.data?.items?.created_at
                )
                setHandoverdate(res?.data?.items?.updated_at
                )
                setUploadedby(res?.data?.items?.uploded_by_name)

            })
            .catch(error => {
                Toast.show(
                    error.message || 'An error occurred',
                    Toast.BOTTOM,
                    
                );
                setShowLoader(false);
            });
    };

    return (

        <>
            <View style={styles.headerroot}>

                <TouchableOpacity onPress={() => props.navigation.goBack()}>
                    <Ionicons name="arrow-back" size={26} color={"#fff"} style={styles.arrback} />
                </TouchableOpacity>
                <Text style={styles.headertext}>{itemname}</Text>

            </View>

            <ScrollView>
                <View style={styles.cardroot}>

                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Item Name:</Text>
                        <Text style={styles.values}>{itemname == "" ? <Text>No data found</Text> : itemname}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Category Name:</Text>
                        <Text style={styles.values}>{catnam == "" ? <Text>No data found</Text> : catnam}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>

                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Airline Name:</Text>
                        <Text style={styles.values}>{airlinenam == "" ? <Text>No data found</Text> : airlinenam}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>

                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Item Status:</Text>
                        <Text style={styles.values}>{itemstatus == "" ? <Text>No data found</Text> : itemstatus}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>
                    </View>
                    <View>
                        {showLoader && <Loader />}

                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Flight Number:</Text>
                        <Text style={styles.values}>{flightnum == "" ? <Text>No data found</Text> : flightnum}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Seat Number:</Text>
                        <Text style={styles.values}>{seatno == "" ? <Text>No data found</Text> : seatno}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Stand Number:</Text>
                        <Text style={styles.values}>{standno == "" ? <Text>No data found</Text> : standno}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Descriptions:</Text>
                        <Text style={styles.values}>{des == "" ? <Text>No data found</Text> : des}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: des?.length >= 40 ? 60 : 12 }}>
                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Remark:</Text>
                        <Text style={styles.values}>{remark == "" ? <Text>No data found</Text> : remark}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Item Uploaded By:</Text>
                        <Text style={styles.values}>{uploadedby == "" ? <Text>No data found</Text> : uploadedby}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Item Upload Time:</Text>
                        <Text style={styles.values}>{moment(dateupdate).format('llll')}</Text>

                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View style={{
                        width: "100%", height: 40, backgroundColor: '#D5D8DC', justifyContent: 'center', elevation: 5,  shadowOffset: { width: 1, height: 1 },
                        shadowColor: '#566573',
                        shadowOpacity: 1.0,
                    }}>
                        <Text style={{ alignSelf: 'center', fontSize: 16, color: '#000', fontWeight: 'bold' }}>
                            Handover to
                        </Text>

                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>handover to:</Text>
                        <Text style={styles.values}>{handoverto == "" ? <Text>No data found</Text> : handoverto}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Name:</Text>
                        <Text style={styles.values}>{passengername == "" ? <Text>No data found</Text> : passengername}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Email/Staff Id:</Text>
                        <Text style={styles.values}>{passengeremail == "" ? <Text>No data found</Text> : passengeremail}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Phone:</Text>
                        <Text style={styles.values}>{passengerphone == "" ? <Text>No data found</Text> : passengerphone}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View style={{
                        width: "100%", height: 40, backgroundColor: '#D5D8DC', justifyContent: 'center', elevation: 5,  shadowOffset: { width: 1, height: 1 },
                        shadowColor: '#566573',
                        shadowOpacity: 1.0,
                    }}>
                        <Text style={{ alignSelf: 'center', fontSize: 16, color: '#000', fontWeight: 'bold' }}>
                            Handedover By
                        </Text>

                    </View>

                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Location:</Text>
                        <Text style={styles.values}>{handoverat == "" ? <Text>No data found</Text> : handoverat}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Handover by name:</Text>
                        <Text style={styles.values}>{handoverbyname == "" ? <Text>No data found</Text> : handoverbyname}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>

                    <View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Item handover Time:</Text>
                        <Text style={styles.values}>{moment(handoverdate).format('llll')}</Text>

                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
                    </View>
                    <ScrollView horizontal={true} style={{ alignSelf: 'center' }}>
                        {
                            imgarr?.images?.map((image) => {
                                const selectedImage = imgarr?.images?.find((image) => image.id === selectedImageId);
                                return (
                                    <View style={{
                                    }}>
                                        <Text style={styles.uploadedimgtext}>Uploaded Images</Text>
                                        <TouchableOpacity key={image.id} onPress={() => toggleModal(image?.id)}>
                                            <Image
                                                style={styles.logo}
                                                source={{
                                                    uri: baseimg + "/" + image?.image_name
                                                }}
                                            />
                                        </TouchableOpacity>
                                        {/* modal for logout */}

                                        <View style={{ flex: 1 }}>
                                            <Modal isVisible={isModalVisible}>
                                                <View style={{ alignSelf: 'center' }}>
                                                    <TouchableOpacity onPress={toggleModal}>
                                                        <Entypo style={{ alignSelf: 'flex-end' }} color={colors?.red} name="circle-with-cross" size={40} />
                                                    </TouchableOpacity>
                                                    <Image
                                                        source={{ uri: baseimg + "/" + selectedImage?.image_name }}
                                                        style={{ width: 350, height: 400, resizeMode: 'contain', }}
                                                    />
                                                </View>

                                            </Modal>
                                        </View>
                                        {/* modal for logout */}
                                    </View>

                                )
                            })

                        }
                    </ScrollView>
                    {
                        signimgs == null ? null :
                            <View style={styles.signatureback

                            }>
                                <Text style={styles.signaturetext}>Signature</Text>
                                <Image
                                    style={styles.signimg}
                                    source={{
                                        uri: signimgs == undefined ? "https://www.freeiconspng.com/thumbs/no-image-icon/no-image-icon-15.png" : baseimg + "/" + signimgs
                                    }}
                                />
                            </View>
                    }


                </View>

            </ScrollView>





        </>

    );
};

export default HandOverDetails;

const styles = StyleSheet.create({
    headerroot: {
        width: '100%',
        height: Platform.OS == "android" ? 60 : 100,
        backgroundColor: colors?.blue,
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        flexDirection: 'row',
        // marginTop:50
    },

    headertext: {
        fontSize: 18,
        alignSelf: "center",
        color: "#fff",
        fontWeight: 'bold',
        letterSpacing: 1,
        marginLeft: "10%",
        marginTop: Platform.OS == 'android' ? 10 : 40,
    },

    submitbtnroot: {
        width: '95%',
        height: 50,
        backgroundColor: 'green',
        justifyContent: "center",
        borderRadius: 8,
        alignSelf: 'center',
        marginBottom: 5
    },
    cardroot: {
        width: '95%',
        height: 1400,
        alignSelf: 'center',
        marginTop: 10,
        borderRadius: 12,
        backgroundColor: '#fff',
        elevation: 5,
        marginBottom: 10,
        shadowOffset: { width: 1, height: 1 },
        shadowColor: '#566573',
        shadowOpacity: 1.0,
    },
    values: {
        position: 'absolute',
        alignSelf: "flex-end",
        marginLeft: '30%',
        fontSize: 14,
        fontWeight: 'bold',
        color: 'green', margin: 10,
    }
    ,
    arrback: {
        alignSelf: "center",
        marginLeft: 10,
        marginTop: Platform.OS == 'android' ? 16 : 60,

    },
    logo: {
        resizeMode: 'contain',
        width: "80%", height: 80,
        borderRadius: 5,
        alignSelf: "center",
    },
    signimg: {
        resizeMode: 'contain',
        width: "100%", height: 130, borderBottomLeftRadius: 10, borderBottomRightRadius: 10

    },
    signatureback: {
        width: '86%',
        height: 160,
        backgroundColor: '#fff',
        margin: 12,
        borderRadius: 10,
        elevation: 4,
        borderWidth: 1,
        borderColor: "#D6DBDF",
        alignSelf: 'center',
        shadowOffset: { width: 1, height: 1 },
        shadowColor: '#566573',
        shadowOpacity: 1.0,
    },
    uploadedimgtext: {
        alignSelf: "center",
        color: colors?.blue,
        fontWeight: 'bold',
        marginTop: 20,
        fontSize: 12,
        marginHorizontal: 20,
        marginBottom: 5
    },
    signaturetext: {
        alignSelf: "center",
        color: colors?.blue,
        fontWeight: 'bold',
        marginTop: 5,
        fontSize: 18
    }

});

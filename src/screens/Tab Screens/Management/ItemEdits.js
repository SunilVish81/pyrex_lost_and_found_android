

import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Image, ScrollView, TouchableOpacity, Keyboard, Platform } from 'react-native';
import { Dropdown } from 'react-native-element-dropdown';
import AntDesign from 'react-native-vector-icons/AntDesign';
import colors from '../../../components/colors';
import { TextInput } from 'react-native-element-textinput';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import Ionicons from 'react-native-vector-icons/Ionicons'; // Use appropriate icon library
import { API_BASE_URL } from '../../../ApiBaseSetUp';
import { Loader } from '../../../components/Loader';
import Toast from 'react-native-simple-toast';





const ItemEdits = (props) => {
  const [showLoader, setShowLoader] = useState(true);
  const [itemliststore, setItemliststore] = useState([]);
  const [itemname, setItemname] = useState("");
  const [storeImgBase1, setStoreImgBase1] = useState([]);
  const [storeImgBase2, setStoreImgBase2] = useState([]);
  const [isModalVisiblebeneficial, setModalVisiblebeneficial] = useState(false);
  const [isModalVisiblebeneficial1, setModalVisiblebeneficial1] = useState(false);
  const [openKey, setOpenKey] = useState("");


  const [image, setImage] = useState("https://i.postimg.cc/4yZt5wq8/upload-file.png");
  const [image1, setImage1] = useState("https://i.postimg.cc/4yZt5wq8/upload-file.png");
  const [value2, setValue2] = useState("");
  const [value3, setValue3] = useState("");
  const [value4, setValue4] = useState("");

  const datastatus = [
    {

      label: "In Possession", value: "In Possession",

    },
    {
      label: "Handed over", value: "Handed over",
    },
    {
      label: "Disposed off", value: "Disposed off",
    },
    {
      label: "Others", value: "Others",
    }


  ]


  const [flightno, setFlightno] = useState("");
  const [seatno, setSeatno] = useState("");
  const [standno, setStandno] = useState("");
  const [descriptions, setDescriptions] = useState("");
  const [remark, setRemark] = useState("");
  const [airname, setAirname] = useState([]);
  const [catdata, setCatdata] = useState([]);

  const ModalOpen = () => {
    setModalVisiblebeneficial(!isModalVisiblebeneficial)

  }
  const ModalOpen1 = () => {
    setModalVisiblebeneficial1(!isModalVisiblebeneficial1)

  }


  const Pifromcamimg1 = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
    }).then(image => {
      setImage(image?.path)

      Toast.show(
        'Images Uploaded',
        Toast.BOTTOM,

      );
      const formattedData = [];
      formattedData.push(`data:${image.mime};base64,${image.data}`)
      setStoreImgBase2(formattedData)
      ModalOpen1()
    });
  }

  const PifromGalimg1 = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      multiple: true,
      includeBase64: true,
    }).then(image => {
      setImage1(image?.path)

      Toast.show(
        'Images Uploaded',
        Toast.BOTTOM,

      );

      const formattedData = [];
      image?.map((indx) => {
        formattedData.push(`data:${indx.mime};base64,${indx.data}`)
      })
      setStoreImgBase1(formattedData)
      ModalOpen()
    });
  };
  const Pifromcamimg2 = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
    }).then(image => {
      setStoreImgBase2(`data:${image.mime};base64,${image.data}`)
      setImage1(image?.path)
      ModalOpen1()
    });
  }

  const PifromGalimg2 = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
    }).then(image => {
      setStoreImgBase2(`data:${image.mime};base64,${image.data}`)
      setImage1(image?.path)
      ModalOpen1()
    });
  };

  React.useEffect(() => {
    const focusHandler = props.navigation.addListener('focus', () => {
      airnameget()
      Getdasdata()
      itemlist()
      detailsFetch()
    });
    return focusHandler;
  }, [props]);

  const detailsFetch = async () => {
    setShowLoader(true)

    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/items/details', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },
      body: JSON.stringify({
        item_id: props?.route?.params?.id

      }),

    })
      .then(response => response.json())
      .then(res => {
        setShowLoader(false)


        res?.data?.airlines.map((indx) => {
          setValue3(indx?.id)

        })
        res?.data?.items.map((indx) => {
          setItemname(indx?.item_name)
          setValue2(indx?.category_id)
          setValue4(indx?.item_status)
          setFlightno(indx?.flight_no)
          setSeatno(indx?.seat_no)
          setStandno(indx?.stand_no)
          setDescriptions(indx?.item_description)
          setRemark(indx?.remark)

        })
      })
      .catch(error => {
        Toast.show(
          error.message || 'An error occurred',
          Toast.BOTTOM,

        );
        setShowLoader(false);
      });
  };


  const airnameget = async () => {

    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/airlines', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },

    })
      .then(response => response.json())
      .then(res => {
        setAirname(res?.data)

      })
      .catch(error => {
        Toast.show(
          error.message || 'An error occurred',
          Toast.BOTTOM,

        );
        setShowLoader(false);
      });
  };


  const Getdasdata = async () => {

    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/item/categories', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },
    })
      .then(response => response.json())
      .then(res => {
        setCatdata(res?.data)
      })
      .catch(error => {
        Toast.show(
          error.message || 'An error occurred',
          Toast.BOTTOM,

        );
        setShowLoader(false);
      });
  };

  const itemlist = async () => {
    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/unhandover/item/list', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },

    })
      .then(response => response.json())
      .then(res => {
        setItemliststore(res?.data)

      })
      .catch(error => {
        Toast.show(
          error.message || 'An error occurred',
          Toast.BOTTOM,

        );
        setShowLoader(false);
      });
  };


  const airrr = airname?.map(item => ({
    label: item?.airline_name,
    value: item?.id
  }));

  const categoryy = catdata?.map(item => ({
    label: item?.category_name,
    value: item?.id
  }));


  const OnSubmit = async () => {
    const Concats = storeImgBase1.concat(storeImgBase2)
    const Token = await AsyncStorage.getItem('token')
    setShowLoader(true);

    await fetch(API_BASE_URL + 'user/items/update', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },
      body: JSON.stringify({
        item_id: props?.route?.params?.id,
        item_name: itemname,
        item_description: descriptions,
        category_id: value2,
        airline_id: value3,
        item_status: value4,
        flight_no: flightno,
        seat_no: seatno,
        stand_no: standno,
        item_first_images: Concats,
      }),
    })
      .then(response => response.json())
      .then(res => {
        setShowLoader(false);
        if (res?.success === true) {
          props.navigation.navigate('Management')
          Toast.show(
            res?.message,
            Toast.BOTTOM,

          );

        } else if (res?.success === false) {
          Toast.show(
            res?.message,
            Toast.BOTTOM,

          );
          setShowLoader(false);
        }
      })
      .catch(error => {
        Toast.show(
          error.message || 'An error occurred',
          Toast.BOTTOM,

        );
        setShowLoader(false);
      });
  };



  const renderItem2 = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {item.value2 === value2 && (
          <AntDesign
            style={styles.icon}
            color="black"
            name="Safety"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderItem3 = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {item.value3 === value3 && (
          <AntDesign
            style={styles.icon}
            color="black"
            name="Safety"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderItem4 = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {item.value4 === value4 && (
          <AntDesign
            style={styles.icon}
            color="black"
            name="Safety"
            size={20}
          />
        )}
      </View>
    );
  };
  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => {
      setOpenKey(1)
    });

    const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setOpenKey(2)
    });

    return () => {
      keyboardDidShowListener.remove();
      keyboardDidHideListener.remove();
    };
  }, []);

  return (

    <>
      <View style={{ flex: 1, marginBottom: Platform.OS == "ios" && openKey == 1 ? 320 : null }}>

        <View style={styles.headerroot}>
          <TouchableOpacity onPress={() => props.navigation.navigate("Management")}>
            <Ionicons name="arrow-back" size={26} color={"#fff"} style={styles.arrback} />
          </TouchableOpacity>
          <Text style={styles.headertext}>{itemname}</Text>

        </View>
        <ScrollView>
          <View style={styles.maincardroot}>
            <View>
              <Text style={styles.editandupdatetext}>Edit and Updates</Text>
            </View>
            <View style={styles.line}>
            </View>
            <View style={{ marginTop: 15 }}>

              <Dropdown
                style={styles.dropdown2}
                placeholderStyle={styles.placeholderStyle}
                selectedTextStyle={styles.selectedTextStyle}
                inputSearchStyle={styles.inputSearchStyle}
                iconStyle={styles.iconStyle}
                data={categoryy}
                containerStyle={{ backgroundColor: "#EBEDEF" }}

                search
                maxHeight={300}
                labelField="label"
                valueField="value"
                placeholder="Item Category"
                searchPlaceholder="Search..."
                value={value2}
                onChange={item => {
                  setValue2(item.value);
                }}

                renderItem={renderItem2}
              />
            </View>

            <Dropdown
              style={styles.dropdown2}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              iconStyle={styles.iconStyle}
              containerStyle={{ backgroundColor: "#EBEDEF" }}

              data={airrr}
              search
              maxHeight={300}
              labelField="label"
              valueField="value"
              placeholder="AirLine Name"
              searchPlaceholder="Search..."
              value={value3}
              onChange={item => {
                setValue3(item.value);
              }}

              renderItem={renderItem3}
            />
            <Dropdown
              style={styles.dropdown2}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              iconStyle={styles.iconStyle}
              containerStyle={{ backgroundColor: "#EBEDEF" }}

              data={datastatus}
              search
              maxHeight={300}
              labelField="label"
              valueField="value"
              placeholder="Item Status"
              searchPlaceholder="Search..."
              value={value4}
              onChange={item => {
                setValue4(item.value);
              }}

              renderItem={renderItem4}
            />

            <View>

              <TextInput
                value={itemname}
                style={styles.itemnames}
                inputStyle={styles.inputStyle}
                labelStyle={styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                label="Item Name"
                placeholder="Enter Item Name"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setItemname(text);
                }}
              />
            </View>
            <View>

              <TextInput
                value={flightno}
                style={styles.emaiinput}
                inputStyle={styles.inputStyle}
                labelStyle={styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                label="Flight Number"
                placeholder="Enter Flight Number"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setFlightno(text);
                }}
              />
            </View>

            <View>
              <TextInput
                value={seatno}
                style={styles.seatno}
                inputStyle={styles.inputStyle}
                labelStyle={styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                label="Seat Number"
                placeholder=" Enter Seat Number"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setSeatno(text);
                }}
              />
              {showLoader && <Loader />}

            </View>

            <View>

              <TextInput
                value={standno}
                style={styles.standno}
                inputStyle={styles.inputStyle}
                labelStyle={styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                label="Stand Number"
                placeholder="Enter Stand Number"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setStandno(text);
                }}
              />
            </View>
            <View>

              <TextInput
                value={descriptions}
                style={styles.handoveraddress}
                inputStyle={styles.inputStyle}
                labelStyle={Platform.OS == "ios" ? styles.labelStyle1 : styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                multiline={true}
                label="Descriptions"
                placeholder="Enter Descriptions"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setDescriptions(text);
                }}
              />
            </View>

            <View>

              <TextInput
                value={remark}
                style={styles.handoveraddress}
                inputStyle={styles.inputStyle}
                labelStyle={Platform.OS == "ios" ? styles.labelStyle1 : styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                multiline={true}
                label="Remark"
                placeholder="Enter Remark"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setRemark(text);
                }}
              />
            </View>


            {/* modal added  */}

            <View>
              <Modal isVisible={isModalVisiblebeneficial}
                animationType="slide"
              >
                <View>


                  <TouchableOpacity onPress={PifromGalimg1}>
                    <View style={styles.choosefromgaltext}>
                      <Text style={styles.modaltext}>Choose From Gallery</Text>

                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={ModalOpen} >
                    <View style={styles.closetextroot}>
                      <Text style={styles.closetext}>Close</Text>

                    </View>
                  </TouchableOpacity>
                </View>
              </Modal>
            </View>


            {/* modal added end  */}
            {/* modal added  */}

            <View>
              <Modal isVisible={isModalVisiblebeneficial1}
                animationType="slide"
              >
                <View>


                  <TouchableOpacity onPress={Pifromcamimg1}>
                    <View style={styles.choosfromtext}>
                      <Text style={styles.modaltext}>Choose From Camera</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={ModalOpen1} >
                    <View style={styles.closetextroot}>
                      <Text style={styles.closetext}>Close</Text>

                    </View>
                  </TouchableOpacity>
                </View>
              </Modal>
            </View>


            {/* modal added end  */}

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>

              <View style={{
                width: '40%', height: 100, backgroundColor: '#ffff', margin: 12, borderRadius: 10, elevation: 4, borderWidth: 1, borderColor: colors?.blue, shadowOffset: { width: 1, height: 1 },
                shadowColor: '#566573',
                shadowOpacity: 1.0,
              }}>
                <Text style={{ alignSelf: "center", color: colors?.blue, fontWeight: 'bold', marginTop: 5, fontSize: 12 }}>Gallery</Text>
                <TouchableOpacity onPress={ModalOpen}>
                  <Image
                    style={styles.logo}
                    source={{
                      uri: image1,
                    }}
                  />
                </TouchableOpacity>
              </View>
              <View style={{
                width: '40%', height: 100, backgroundColor: '#ffff', margin: 12, borderRadius: 10, elevation: 4, borderWidth: 1, borderColor: colors?.blue, shadowOffset: { width: 1, height: 1 },
                shadowColor: '#566573',
                shadowOpacity: 1.0,
              }}>
                <Text style={{ alignSelf: "center", color: colors?.blue, fontWeight: 'bold', marginTop: 5, fontSize: 12 }}>Camera</Text>
                <TouchableOpacity onPress={ModalOpen1}>
                  <Image
                    style={styles.logo}
                    source={{
                      uri: image,
                    }}
                  />
                </TouchableOpacity>
              </View>

            </View>
          </View>
          <TouchableOpacity onPress={OnSubmit} style={{ width: '95%', height: 50, backgroundColor: 'green', justifyContent: "center", borderRadius: 8, alignSelf: 'center', marginBottom: 5 }}>
            <Text style={{ alignSelf: "center", fontSize: 18, color: "#fff", fontWeight: 'bold', letterSpacing: 1 }}>Next</Text>
          </TouchableOpacity>
          {
            Platform.OS == "android" ? null :
              <View style={{ width: '100%', height: 20 }}>

              </View>
          }
        </ScrollView>
      </View>
    </>

  );
};

export default ItemEdits;

const styles = StyleSheet.create({
  dropdown: {
    margin: 16,
    height: 50,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    borderWidth: 1,
    borderWidth: 1,
    borderColor: colors?.blue,

  },
  dropdown2: {
    margin: 16,
    height: 50,

    backgroundColor: 'white',
    borderRadius: 8,
    padding: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: -5,
    borderWidth: 1,
    borderColor: colors?.blue,
  },
  dropdownhandoverby: {
    margin: 16,
    height: 50,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 12,
    borderWidth: 1,
    borderColor: colors?.blue,
  },


  icon: {
    alignSelf: "center",
  },
  item: {
    padding: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  placeholderStyle: {
    fontSize: 16,
  },
  selectedTextStyle: {
    fontSize: 16,
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 16,
  },

  headerroot: {
    width: '100%',
    height: Platform.OS == "android" ? 60 : 100,
    backgroundColor: colors?.blue,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    flexDirection: 'row',
  },

  headertext: {
    fontSize: 18,
    alignSelf: "center",
    color: "#fff",
    fontWeight: 'bold',
    letterSpacing: 1,
    marginLeft: "10%",
    marginTop: Platform.OS == 'android' ? 10 : 40,

    // marginLeft: '20%'
  },

  emaiinput: {
    height: 50,
    width: "92%",
    paddingHorizontal: 12,
    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    marginTop: -5,
    borderWidth: 1,
    borderColor: colors?.blue,

  },
  itemnames: {
    height: 50,
    width: "92%",
    paddingHorizontal: 12,
    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    marginTop: -5,
    borderWidth: 1,
    borderColor: colors?.blue,
    marginBottom: 15

  },
  seatno: {
    height: 50,
    width: "92%",
    paddingHorizontal: 12,
    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    marginTop: 10,
    borderWidth: 1,
    borderColor: colors?.blue,

  },
  standno: {
    height: 50,
    width: "92%",
    paddingHorizontal: 12,
    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    marginTop: 10,
    borderWidth: 1,
    borderColor: colors?.blue,

  },
  handoveraddress: {
    height: 80,
    paddingHorizontal: 12,
    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    borderWidth: 1,
    borderColor: colors?.blue,
    marginTop: 10,
    width: "92%",


  },
  inputStyle: { fontSize: 16, margin: -10, marginLeft: 0 },
  labelStyle: { fontSize: 16, color: colors?.blue, marginTop: -5, fontWeight: 'bold' },
  labelStyle1: { fontSize: 16, color: colors?.blue, marginTop: -5, fontWeight: 'bold', marginBottom: 10 },
  placeholderStyle: { fontSize: 16 },
  textErrorStyle: {
    fontSize: 16
  },
  logo: {
    resizeMode: 'contain',
    width: " 100%", height: 60
  },
  editandupdatetext: {
    fontSize: 16,
    color: "#000",
    fontWeight: "bold",
    marginLeft: 22,
    marginTop: 10
  },
  maincardroot: {
    width: '95%',
    height: 780,
    backgroundColor: "#fff",
    marginTop: 10,
    alignSelf: "center",
    elevation: 4,
    borderRadius: 8,
    marginBottom: 5
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: colors?.blue,
    marginTop: 12,
    opacity: 0.4
  },
  arrback: {
    alignSelf: "center",
    marginLeft: 10,
    marginTop: Platform.OS == 'android' ? 16 : 58,
  },

  choosfromtext: {
    width: 180,
    height: 40,
    backgroundColor: "green",
    alignSelf: 'center',
    borderRadius: 10,
    justifyContent: 'center'
  },
  choosefromgaltext: {
    width: 180,
    height: 40,
    backgroundColor: colors?.blue,
    alignSelf: 'center',
    marginTop: 10,
    borderRadius: 10,
    justifyContent: 'center'
  },
  modaltext: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 12,
    fontWeight: '500'
  },
  closetextroot: {
    width: 150,
    height: 40,
    backgroundColor: "red",
    alignSelf: 'center',
    marginTop: 10,
    borderRadius: 10,
    justifyContent: 'center'
  },
  closetext: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 12,
    fontWeight: '500'
  },
  uploadetailstext: {
    fontSize: 16,
    color: "#000",
    fontWeight: "bold",
    marginLeft: 22,
    marginTop: 10
  },



});
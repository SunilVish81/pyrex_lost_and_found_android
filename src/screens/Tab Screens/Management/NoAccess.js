import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'

const NoAccess = () => {
    return (
        <View style={{ flex: 1, justifyContent:'center' }}>
<Text style={{alignSelf:'center', fontSize:18, fontWeight:'bold', color:'#000'}}>You haven't Permission to acces this tab !</Text>
            <Image
                style={styles.img}
                source={require('../../../assets/no-entry.png')}
            />  
              </View>
    )
}

export default NoAccess

const styles = StyleSheet.create({
    img:{
        width:120,
        height:120,
        resizeMode:'contain',
        alignSelf:"center",
        marginTop:20
    }
})
import { StyleSheet, Text, View, useAnimatedValue, TouchableOpacity, ScrollView, Image, Platform } from 'react-native'
import React, { useEffect, useState } from 'react'
import { TextInput } from 'react-native-element-textinput';
import colors from '../../../../components/colors';
import { Dropdown } from 'react-native-element-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons'; // Use appropriate icon library
import AsyncStorage from '@react-native-community/async-storage';
import { Loader } from '../../../../components/Loader';
import { API_BASE_URL } from '../../../../ApiBaseSetUp';
import Modal from "react-native-modal";
import Toast from 'react-native-simple-toast';





const UpdateUser = (props) => {
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [phone, setPhone] = useState("")
    const [newpassword, setNewPassword] = useState("")
    const [confirmpassword, setConfirmpassword] = useState("")
    const [value1, setValue1] = useState("");
    const [showLoader, setShowLoader] = useState(false);
    const [roleuser, setRoleuser] = useState([]);
    const [isModalVisible, setModalVisible] = useState(false);

    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };

    const renderItem1 = item => {
        return (
            <View style={styles.item}>
                <Text style={styles.textItem}>{item.label}</Text>
                {item.value1 === value1 && (
                    <AntDesign
                        style={styles.icon}
                        color="black"
                        name="Safety"
                        size={20}
                    />
                )}
            </View>
        );
    };


    useEffect(() => {
        getroles()
    }, [])




    const getroles = async () => {
        const Token = await AsyncStorage.getItem('token')
        await fetch(API_BASE_URL + 'user/get/roles', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${Token}`,

            },

        })
            .then(response => response.json())
            .then(res => {
                setRoleuser(res?.data)

            })
            .catch(error => {
                Toast.show(
                    error.message || 'An error occurred',
                    Toast.BOTTOM,

                );
                setShowLoader(false);
            });
    };

    const rolesdata = roleuser?.map(item => ({
        label: item?.RoleName,
        value: item?.id
    }));


    const UpdateUser = async () => {
        const Token = await AsyncStorage.getItem('token')
        setShowLoader(true);
        await fetch(API_BASE_URL + 'user/user/update', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${Token}`,

            },
            body: JSON.stringify({
                user_id: props?.route?.params?.id,
                name: name,
                email: email,
                phone: phone,
                role_id: value1
            }),
        })
            .then(response => response.json())
            .then(res => {
                setShowLoader(false);
                if (res?.success === true) {
                    props.navigation.navigate('Users')
                    Toast.show(
                        res?.message,
                        Toast.BOTTOM,

                    );

                } else if (res?.success === false) {
                    Toast.show(
                        res?.message,
                        Toast.BOTTOM,

                    );
                    setShowLoader(false);
                }
            })
            .catch(error => {
                Toast.show(
                    error.message || 'An error occurred',
                    Toast.BOTTOM,

                );
                setShowLoader(false);
            });
    };


    React.useEffect(() => {
        const focusHandler = props.navigation.addListener('focus', () => {
            getuserdetails()
        });
        return focusHandler;
    }, [props]);

    const getuserdetails = async () => {
        const Token = await AsyncStorage.getItem('token')
        setShowLoader(true);
        await fetch(API_BASE_URL + 'user/get/user/details', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${Token}`,

            },
            body: JSON.stringify({
                user_id: props?.route?.params?.id,
            }),
        })
            .then(response => response.json())
            .then(res => {
                setShowLoader(false);
                if (res?.success === true) {
                    setName(res?.data?.name)
                    setEmail(res?.data?.email)
                    setPhone(res?.data?.phone)
                    setValue1(res?.data?.role_id)


                } else if (res?.success === false) {
                    Toast.show(
                        res?.message,
                        Toast.BOTTOM,

                    );
                    setShowLoader(false);
                }
            })
            .catch(error => {
                Toast.show(
                    error.message || 'An error occurred',
                    Toast.BOTTOM,

                );
                setShowLoader(false);
            });
    };
    const changepassword = async () => {
        if (newpassword == "") {
            Toast.show(
                "Please enter new password",
                Toast.BOTTOM,

            );
            return;

        }
        const Token = await AsyncStorage.getItem('token')
        setShowLoader(true);
        await fetch(API_BASE_URL + 'user/password/update', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${Token}`,

            },
            body: JSON.stringify({
                user_id: props?.route?.params?.id,
                new_password: newpassword
            }),
        })
            .then(response => response.json())
            .then(res => {
                setShowLoader(false);
                if (res?.success === true) {
                    toggleModal()
                    Toast.show(
                        res?.message,
                        Toast.BOTTOM,

                    );


                } else if (res?.success === false) {
                    Toast.show(
                        res?.message,
                        Toast.BOTTOM,

                    );
                    setShowLoader(false);
                }
            })
            .catch(error => {
                Toast.show(
                    error.message || 'An error occurred',
                    Toast.BOTTOM,

                );
                setShowLoader(false);
            });
    };

    return (
        <View style={{ flex: 1 }}>
            {/* modal>>>> start */}
            <View>
                <Modal isVisible={isModalVisible}>
                    <View style={styles.modalroot}>
                        <TouchableOpacity onPress={toggleModal}>
                            <Image
                                style={styles.cross}
                                source={{
                                    uri: 'https://i.postimg.cc/tg9MycsF/close.png',
                                }}
                            />
                        </TouchableOpacity>
                        <View style={{ marginTop: 10 }}>
                            <Text style={styles.changepasstext}>Change Password</Text>
                            <TextInput
                                value={newpassword}
                                style={styles.newpassword}
                                inputStyle={styles.inputStyle}
                                labelStyle={styles.labelStyle}
                                placeholderStyle={styles.placeholderStyle}
                                textErrorStyle={styles.textErrorStyle}
                                label="New Password"
                                placeholder="Enter New Password"
                                placeholderTextColor="gray"
                                onChangeText={text => {
                                    setNewPassword(text);
                                }}
                            />
                        </View>
                        <View>
                            <TouchableOpacity onPress={changepassword} style={styles.submitbtnroot}>
                                <Text style={{ alignSelf: "center", fontSize: 18, color: "#fff", fontWeight: 'bold', letterSpacing: 1 }}>Change Password</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                {/* modal>>>> end */}

            </View>
            <View style={styles.headerroot}>
                <TouchableOpacity onPress={() => props.navigation.navigate("Users")}>
                    <Ionicons name="arrow-back" size={26} color={"#fff"} style={styles.arrback} />
                </TouchableOpacity>
                <Text style={styles.headertext}>Update Users</Text>

            </View>
            <ScrollView>
                <View style={{ marginTop: 10 }}>
                    <TextInput
                        value={name}
                        style={styles.handoveraddress}
                        inputStyle={styles.inputStyle}
                        labelStyle={styles.labelStyle}
                        placeholderStyle={styles.placeholderStyle}
                        textErrorStyle={styles.textErrorStyle}
                        label="Name"
                        placeholder="Enter Your Name"
                        placeholderTextColor="gray"
                        onChangeText={text => {
                            setName(text);
                        }}
                    />
                </View>
                <View style={{ marginTop: 10 }}>
                    <TextInput
                        value={email}
                        style={styles.handoveraddress}
                        inputStyle={styles.inputStyle}
                        labelStyle={styles.labelStyle}
                        placeholderStyle={styles.placeholderStyle}
                        textErrorStyle={styles.textErrorStyle}
                        keyboardType='email-address'
                        label="Email"
                        placeholder="Enter Your Email"
                        placeholderTextColor="gray"
                        onChangeText={text => {
                            setEmail(text);
                        }}
                    />
                </View>
                <View style={{ marginTop: 10 }}>
                    <TextInput
                        value={phone}
                        style={styles.handoveraddress}
                        inputStyle={styles.inputStyle}
                        labelStyle={styles.labelStyle}
                        placeholderStyle={styles.placeholderStyle}
                        textErrorStyle={styles.textErrorStyle}
                        label="Phone"
                        keyboardType="number-pad"
                        placeholder="Enter Phone"
                        placeholderTextColor="gray"
                        onChangeText={text => {
                            setPhone(text);
                        }}
                    />
                </View>

                {showLoader && <Loader />}


                <View style={{ marginTop: -5 }}>
                    <Dropdown
                        style={styles.dropdown}
                        placeholderStyle={styles.placeholderStyle}
                        selectedTextStyle={styles.selectedTextStyle}
                        inputSearchStyle={styles.inputSearchStyle}
                        iconStyle={styles.iconStyle}
                        containerStyle={{ backgroundColor: "#EBEDEF" }}
                        data={rolesdata}
                        search
                        maxHeight={300}
                        labelField="label"
                        valueField="value"
                        placeholder="Select role..."
                        searchPlaceholder="Search..."
                        value={value1}
                        onChange={item => {
                            setValue1(item.value);
                        }}
                        renderItem={renderItem1}
                    />
                </View>

                <View>
                    <TouchableOpacity onPress={toggleModal} style={styles.passwordchanges}>
                        <Text style={{ alignSelf: "center", fontSize: 18, color: "#fff", fontWeight: 'bold', letterSpacing: 1 }}>Chanage your Password</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            <View>
                <TouchableOpacity onPress={UpdateUser} style={styles.submitbtnroot}>
                    <Text style={{ alignSelf: "center", fontSize: 18, color: "#fff", fontWeight: 'bold', letterSpacing: 1 }}>Update User</Text>
                </TouchableOpacity>
            </View>
            {
                Platform.OS == "android" ? null :
                    <View style={{ width: '100%', height: 20 }}>

                    </View>
            }

        </View>
    )
}

export default UpdateUser

const styles = StyleSheet.create({
    handoveraddress: {
        height: 50,
        paddingHorizontal: 12,
        borderRadius: 8,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        alignSelf: "center",
        borderWidth: 1,
        borderColor: colors?.blue,
        width: "92%",


    },
    newpassword: {
        height: 50,
        paddingHorizontal: 12,
        borderRadius: 8,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        alignSelf: "center",
        borderWidth: 1,
        borderColor: colors?.blue,
        width: "92%",
        marginBottom: 50


    },
    inputStyle: { fontSize: 16, margin: -10, marginLeft: 0 },
    labelStyle: { fontSize: 16, color: colors?.blue, marginTop: -5, fontWeight: 'bold', },
    placeholderStyle: { fontSize: 16 },
    textErrorStyle: {
        fontSize: 16
    },
    dropdown: {
        margin: 16,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 8,
        padding: 12,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        borderWidth: 1,
        borderWidth: 1,
        borderColor: colors?.blue,
    },

    dropdownhandoverby: {
        margin: 16,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 8,
        padding: 12,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        marginTop: 12,
        borderWidth: 1,
        borderColor: colors?.blue,
    },


    icon: {
        marginRight: 5,
    },
    item: {
        padding: 17,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textItem: {
        flex: 1,
        fontSize: 16,
    },
    placeholderStyle: {
        fontSize: 16,
    },
    selectedTextStyle: {
        fontSize: 16,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    },

    headerroot: {
        width: '100%',
        height: 60,
        backgroundColor: colors?.blue,
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        justifyContent: 'center'
    },

    headertext: {
        fontSize: 18,
        alignSelf: "center",
        marginTop: 10,
        color: "#fff",
        fontWeight: 'bold',
        letterSpacing: 1
    },
    container: {
        padding: 16,
    },
    nameinput: {
        height: 50,
        paddingHorizontal: 12,
        borderRadius: 8,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        alignSelf: "center",
        marginTop: -4,
        borderWidth: 1,
        borderColor: colors?.blue,
        width: "92%"

    },
    headerroot: {
        width: '100%',
        height: Platform.OS == "android" ? 60 : 100,
        backgroundColor: colors?.blue,
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        flexDirection: 'row',
    },

    headertext: {
        fontSize: 18,
        alignSelf: "center",
        color: "#fff",
        fontWeight: 'bold',
        letterSpacing: 1,
        marginLeft: "10%",
        marginTop: Platform.OS == 'android' ? 10 : 40,

        // marginLeft: '20%'
    },
    arrback: {
        alignSelf: "center",
        marginLeft: 10,
        marginTop: 18,
        marginTop: Platform.OS == 'android' ? 16 : 60,

    },
    submitbtnroot: {
        width: '95%',
        height: 50,
        backgroundColor: 'green',
        justifyContent: "center",
        borderRadius: 8,
        alignSelf: 'center',
        marginBottom: 5
    },
    passwordchanges: {
        width: '95%',
        height: 50,
        backgroundColor: colors?.blue,
        justifyContent: "center",
        borderRadius: 8,
        alignSelf: 'center',
        marginBottom: 5
    },
    modalroot: {
        width: '90%',
        height: 250,
        backgroundColor: '#fff',
        alignSelf: "center",
        borderRadius: 5,
        justifyContent: "center",
        marginTop: Platform.OS == "ios" ? -80 : null
    },
    changepasstext: {
        alignSelf: "center",
        fontSize: 18,
        color: colors?.blue,
        marginBottom: 10,
        fontWeight: "bold",
        letterSpacing: 2,
        marginTop: -10
    },
    cross: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
        alignSelf: 'flex-end',
        right: 5
    }
})
import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, Image, StyleSheet, TouchableOpacity, RefreshControl, Linking, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'; // Use appropriate icon library
import AntDesign from 'react-native-vector-icons/AntDesign'; // Use appropriate icon library
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; // Use appropriate icon library
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'; // Use appropriate icon library
import colors from '../../../components/colors';
import AsyncStorage from '@react-native-community/async-storage';
import { Loader } from '../../../components/Loader';
import { API_BASE_URL } from '../../../ApiBaseSetUp';
import moment from 'moment';
import Modal from "react-native-modal";
import { Calendar, LocaleConfig } from 'react-native-calendars';
import { Dropdown } from 'react-native-element-dropdown';
import { TextInput } from 'react-native-element-textinput';
import Toast from 'react-native-simple-toast';



const Item = (props) => {
  const [showLoader, setShowLoader] = useState(false);
  const [storedata, setStoredata] = useState([]);
  const [flightnumber, setFlightNumber] = useState("");
  const [refreshing, setRefreshing] = useState(false);
  const [search, setSearch] = useState('');
  const [filteredDataSource, setFilteredDataSource] = useState([]);
  const [masterDataSource, setMasterDataSource] = useState([]);
  const [baseimg, setBaseImg] = useState("");




  const [value1, setValue1] = useState("");
  const [value2, setValue2] = useState("");
  const [value3, setValue3] = useState("");
  const [downloadurl, setDownloadurl] = useState("");
  const [filename, setFilename] = useState("");
  const [filterdata, setFilterdata] = useState([]);

  const [catdata, setCatdata] = useState([]);
  const [airname, setAirname] = useState([]);


  const [isModalVisibleto, setModalVisibleto] = useState(false);
  const [isModalVisiblefrom, setModalVisiblefrom] = useState(false);

  const [selected, setSelected] = useState('');
  const [selectedto, setSelectedto] = useState('');
  const [isModalVisible, setModalVisible] = useState(false);

  const [isModalVisibledown, setModalVisibledown] = useState(false);
  const [isModaldownloadpress, setModaldownloadpress] = useState(false);


  const datastatus = [
    {
      id: 1,

      label: "In Possession", value: "In Possession",

    },
    {
      id: 2,
      label: "Handed over", value: "Handed over",
    },
    {
      id: 3,
      label: "Disposed off", value: "Disposed off",
    },
    {
      id: 4,
      label: "Others", value: "Others",
    }


  ]
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  const toggleModalto = () => {
    setModalVisibleto(!isModalVisibleto);
  };
  const toggleModalfrom = () => {
    setModalVisiblefrom(!isModalVisiblefrom);
  };
  const toggleModalfromdownload = () => {
    setModalVisibledown(!isModalVisibledown);
  };
  const toggleModaldownloadpress = () => {
    setModaldownloadpress(!isModaldownloadpress);
  };

  const renderItem1 = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {item.value1 === value1 && (
          <AntDesign
            style={styles.icon}
            color="black"
            name="Safety"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderItem2 = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {item.value2 === value2 && (
          <AntDesign
            style={styles.icon}
            color="black"
            name="Safety"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderItem3 = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {item.value3 === value3 && (
          <AntDesign
            style={styles.icon}
            color="black"
            name="Safety"
            size={20}
          />
        )}
      </View>
    );
  };

  React.useEffect(() => {
    const focusHandler = props.navigation.addListener('focus', () => {
      Getcatdata()
      airnameget()
    });
    return focusHandler;
  }, [props]);
  const Getcatdata = async () => {

    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/item/categories', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,
      },
    })
      .then(response => response.json())
      .then(res => {
        setCatdata(res?.data)
      })
      .catch(error => {
        Toast.show(error?.message, Toast.BOTTOM);
        setShowLoader(false);
      });
  };


  const airnameget = async () => {
    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/airlines', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },

    })
      .then(response => response.json())
      .then(res => {
        setBaseImg(res?.image_base_url)
        setAirname(res?.data)

      })
      .catch(error => {
        Toast.show(error?.message, Toast.BOTTOM);
        setShowLoader(false);
      });
  };
  const airrr = airname?.map(item => ({
    label: item?.airline_name,
    value: item?.id
  }));

  const categoryy = catdata?.map(item => ({
    label: item?.category_name,
    value: item?.id
  }));

  const renderItem = ({ item }) => {
    const rootchnage = () => {
      if (item?.handover_to !== null) {
        props?.navigation.navigate("HandOverDetails", {
          id: item?.id
        })
        return;
      } else {
        props?.navigation.navigate("ItemDetails", {
          id: item?.id

        })
        return;
      }

    }



    return (

      <TouchableOpacity onPress={rootchnage} style={styles.card}>

        <Image
          style={styles.image}
          source={{
            uri: item?.ItemImage == null ? "https://i.postimg.cc/C1MBVZy9/NOIMG.jpg" : baseimg + "/" + item?.ItemImage,
          }}
        />
        <View style={styles.textContainer}>
          <Text style={styles.name}>{item.item_name}</Text>
          <View style={styles.infoContainer}>
            <Icon name="tag" size={14} color={colors?.blue} style={styles.icon} />
            <Text style={styles.category}>{item.category_name}</Text>
          </View>

          <View style={styles.infoContainer}>
            <MaterialCommunityIcons name="list-status" size={14} color={colors?.blue} style={styles.icon} />
            <Text style={styles.flightNumber1}>{item.item_status}</Text>
          </View>

          <View style={styles.infoContainer}>
            <Icon name="calendar" size={14} color={colors?.blue} style={styles.icon} />
            <Text style={styles.date}>{moment(item.created_at).format('llll')}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  React.useEffect(() => {
    const focusHandler = props.navigation.addListener('focus', () => {
      Getdasdata()
    });
    return focusHandler;
  }, [props]);


  const Getdasdata = async () => {

    setShowLoader(true);

    const Token = await AsyncStorage.getItem('token')
    setShowLoader(true);
    await fetch(API_BASE_URL + 'user/get/items', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },

    })
      .then(response => response.json())
      .then(res => {
        setFilteredDataSource(res?.data);
        setMasterDataSource(res?.data);
        setShowLoader(false)
      })
      .catch(error => {
        Toast.show(error?.message, Toast.BOTTOM);
        setShowLoader(false);
      });
  };

  setTimeout(() => {
    setRefreshing(false);
  }, 1000);

  const handleRefresh = () => {
    setRefreshing(true);
    Getdasdata()
  };

  // filter Api Call >>>>>>>>>
  const GetFilterData = async () => {
    if (selected == "" && selectedto == "" && value1 == "" && value2 == "" && value3 == "" && flightnumber == "") {
      Toast.show("No any fields selected.", Toast.BOTTOM);
      return;
    }
    if (selected !== "") {
      if (selectedto == "") {
        Toast.show("Please select to date.", Toast.BOTTOM);
        return;
      }
    }
    setShowLoader(true);

    const Token = await AsyncStorage.getItem('token')

    fetch(API_BASE_URL + `user/get/items?from_date=${selected}&to_date=${selectedto}&category_id=${value2}&airline_id=${value1}&flight_no=${flightnumber}&item_status=${value3}&po=${"get_data"}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        setShowLoader(false);
        setFilteredDataSource(data?.data);
        setMasterDataSource(data?.data);
        toggleModal()
        Toast.show(
          data?.message,
          Toast.BOTTOM,

        );

      })
      .catch(error => {
        Toast.show(error, Toast.BOTTOM);
        setShowLoader(false)

      });
  }
  // filter Api Call >>>>>>>>>

  const cleardata = () => {
    setSelected("")
    setSelectedto("")
    setValue1("")
    setValue2("")
    setValue3("")
    setFlightNumber("")

  }

  const downloaddata = async () => {
    if (selected == "" && selectedto == "" && value1 == "" && value2 == "" && value3 == "" && flightnumber == "") {
      Toast.show("No any fields selected.", Toast.BOTTOM);
      return;

    }
    if (selected !== "") {
      if (selectedto == "") {
        Toast.show("Please select to date.", Toast.BOTTOM);
        return;
      }
    }
    setShowLoader(true);

    const Token = await AsyncStorage.getItem('token')

    fetch(API_BASE_URL + `user/get/items?from_date=${selected}&to_date=${selectedto}&category_id=${value2}&airline_id=${value1}&flight_no=${flightnumber}&item_status=${value3}&po=${"download_data"}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        setShowLoader(false);
        toggleModaldownloadpress()
        toggleModalfromdownload()
        setDownloadurl(data?.data?.export_file_path)

      })
      .catch(error => {
        Toast.show(error, Toast.BOTTOM);
        setShowLoader(false)

      });
  }



  const downlodpress = () => {
    Linking.openURL(downloadurl);
    toggleModaldownloadpress()
  };

  const searchFilterFunction = (text) => {
    // Check if searched text is not blank
    if (text) {
      // Inserted text is not blank
      // Filter the masterDataSource and update FilteredDataSource
      const newData = masterDataSource.filter(function (item) {
        // Applying filter for the inserted text in search bar
        const itemData = item.item_name + item?.category_name + item?.item_status
          ? item.item_name.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setFilteredDataSource(masterDataSource);
      setSearch(text);
    }
  };


  return (
    <>
      <View style={styles.headerroot}>
        <Text style={styles.headertext}>Items List</Text>
      </View>

      <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
        <TouchableOpacity onPress={toggleModal} >
          <View style={styles.filterbtnroot}>
            <Text style={styles.filtertext}>
              Filter
            </Text>
            <AntDesign name="filter" size={18} color={colors?.blue} style={styles.iconfilter} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={toggleModalfromdownload}>
          <View style={styles.downloadbtnroot}>
            <Text style={styles.downloadtextroot}>
              Download
            </Text>
            <FontAwesome5 name="file-download" size={18} color={colors?.blue} style={styles.iconfilter} />
          </View>
        </TouchableOpacity>
      </View>
      {/* modal */}
      <Modal isVisible={isModalVisible}>
        <View style={styles.filtermodalroot}>
          <View style={styles.modaltoptext}>
            <Text style={styles.filteryourdatatext}>Filter your data </Text>
            <TouchableOpacity onPress={toggleModal}>
              <Text style={styles.canbtntext}>Cancel </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.line}>
          </View>

          {/* item added in modal */}
          <View style={styles.modalroot}>

            <View style={styles.datetextroot}>
              <TouchableOpacity onPress={toggleModalfrom}>
                <View style={{ flexDirection: 'row', alignSelf: 'center', }}>

                  <Text style={styles.fromdatetext}>{selected == '' ? <Text>From date</Text> : selected}</Text>

                  <Image
                    style={styles.calimg}
                    source={require('../../../assets/calendar.png')}
                  />

                </View>
              </TouchableOpacity>

            </View>

            <TouchableOpacity onPress={toggleModalto}>

              <View style={styles.todateroot}>
                <View style={{ flexDirection: 'row', alignSelf: 'center', }}>

                  <Text style={styles.todatetext}>{selectedto == '' ? <Text>To date</Text> : selectedto}</Text>

                  <Image
                    style={styles.calimg}
                    source={require('../../../assets/calendar.png')}
                  />

                </View>

              </View>
            </TouchableOpacity>

          </View>

          {/* drop down added */}
          <View style={{ marginTop: -5 }}>


            <Dropdown
              style={styles.dropdown}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              containerStyle={{ backgroundColor: "#EBEDEF" }}
              iconStyle={styles.iconStyle}
              data={airrr}
              search
              maxHeight={300}
              labelField="label"
              valueField="value"
              placeholder="Select Airline Name"
              searchPlaceholder="Search..."
              value={value1}
              onChange={item => {
                setValue1(item.value);
              }}

              renderItem={renderItem1}
            />
          </View>
          <View style={{ marginTop: -20 }}>


            <Dropdown
              style={styles.dropdown}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              iconStyle={styles.iconStyle}
              containerStyle={{ backgroundColor: "#EBEDEF" }}
              data={categoryy}
              search
              maxHeight={300}
              labelField="label"
              valueField="value"
              placeholder="Select Category"
              searchPlaceholder="Search..."
              value={value2}
              onChange={item => {
                setValue2(item.value);
              }}

              renderItem={renderItem2}
            />
          </View>
          <View style={{ marginTop: -20 }}>


            <Dropdown
              style={styles.dropdown}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              iconStyle={styles.iconStyle}
              containerStyle={{ backgroundColor: "#EBEDEF" }}
              data={datastatus}
              search
              maxHeight={300}
              labelField="label"
              valueField="value"
              placeholder="Select Status"
              searchPlaceholder="Search..."
              value={value3}
              onChange={item => {
                setValue3(item.value);
              }}

              renderItem={renderItem3}
            />
          </View>

          <View style={{ width: "98%", alignSelf: 'center', marginBottom: 15 }}>
            <TextInput
              value={flightnumber}
              style={styles.phoneinput}
              inputStyle={styles.inputStyle}
              labelStyle={styles.labelStyle}
              placeholderStyle={styles.placeholderStyle}
              textErrorStyle={styles.textErrorStyle}
              label="Flight Number"
              placeholder="Enter Flight Number"
              placeholderTextColor="gray"
              onChangeText={text => {
                setFlightNumber(text);
              }}
            />
          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
            <TouchableOpacity onPress={GetFilterData} style={{ width: 100, height: 40, backgroundColor: 'green', justifyContent: 'center', borderRadius: 5 }}>
              <Text style={{ alignSelf: 'center', fontSize: 16, color: '#fff' }}>Apply Filter</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={cleardata} style={{ width: 100, height: 40, backgroundColor: colors?.blue, justifyContent: 'center', borderRadius: 5 }}>
              <Text style={{ alignSelf: 'center', fontSize: 16, color: '#fff' }}>Clear Filter</Text>
            </TouchableOpacity>

          </View>

          {/* item added in modal */}
          {showLoader && <Loader />}

        </View>
      </Modal>

      {/* modal */}
      {/* modal  downloads  */}
      <Modal isVisible={isModalVisibledown}>
        <View style={styles.filtermodalroot}>
          <View style={styles.modaltoptext}>
            <Text style={styles.filteryourdatatext}>Download your data </Text>
            <TouchableOpacity onPress={toggleModalfromdownload}>
              <Text style={styles.canbtntext}>Cancel </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.line}>
          </View>

          {/* item added in modal */}
          <View style={styles.modalroot}>

            <View style={styles.datetextroot}>
              <TouchableOpacity onPress={toggleModalfrom}>
                <View style={{ flexDirection: 'row', alignSelf: 'center', }}>

                  <Text style={styles.fromdatetext}>{selected == '' ? <Text>From date</Text> : selected}</Text>

                  <Image
                    style={styles.calimg}
                    source={require('../../../assets/calendar.png')}
                  />

                </View>
              </TouchableOpacity>

            </View>
            <TouchableOpacity onPress={toggleModalto}>

              <View style={styles.todateroot}>
                <View style={{ flexDirection: 'row', alignSelf: 'center', }}>

                  <Text style={styles.todatetext}>{selectedto == '' ? <Text>To date</Text> : selectedto}</Text>

                  <Image
                    style={styles.calimg}
                    source={require('../../../assets/calendar.png')}
                  />

                </View>

              </View>
            </TouchableOpacity>

          </View>

          {/* drop down added */}
          <View style={{ marginTop: -5 }}>


            <Dropdown
              style={styles.dropdown}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              containerStyle={{ backgroundColor: "#EBEDEF" }}
              iconStyle={styles.iconStyle}
              data={airrr}
              search
              maxHeight={300}
              labelField="label"
              valueField="value"
              placeholder="Select Airline Name"
              searchPlaceholder="Search..."
              value={value1}
              onChange={item => {
                setValue1(item.value);
              }}

              renderItem={renderItem1}
            />
          </View>
          <View style={{ marginTop: -20 }}>


            <Dropdown
              style={styles.dropdown}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              iconStyle={styles.iconStyle}
              containerStyle={{ backgroundColor: "#EBEDEF" }}
              data={categoryy}
              search
              maxHeight={300}
              labelField="label"
              valueField="value"
              placeholder="Select Category"
              searchPlaceholder="Search..."
              value={value2}
              onChange={item => {
                setValue2(item.value);
              }}

              renderItem={renderItem2}
            />
          </View>
          <View style={{ marginTop: -20 }}>


            <Dropdown
              style={styles.dropdown}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              iconStyle={styles.iconStyle}
              containerStyle={{ backgroundColor: "#EBEDEF" }}
              data={datastatus}
              search
              maxHeight={300}
              labelField="label"
              valueField="value"
              placeholder="Select Status"
              searchPlaceholder="Search..."
              value={value3}
              onChange={item => {
                setValue3(item.value);
              }}

              renderItem={renderItem3}
            />
          </View>

          <View style={{ width: "98%", alignSelf: 'center', marginBottom: 15 }}>
            <TextInput
              value={flightnumber}
              style={styles.phoneinput}
              inputStyle={styles.inputStyle}
              labelStyle={styles.labelStyle}
              placeholderStyle={styles.placeholderStyle}
              textErrorStyle={styles.textErrorStyle}
              label="Flight Number"
              placeholder="Enter Flight Number"
              placeholderTextColor="gray"
              onChangeText={text => {
                setFlightNumber(text);
              }}
            />
          </View>


          <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
            <TouchableOpacity onPress={downloaddata} style={{ width: 100, height: 40, backgroundColor: 'green', justifyContent: 'center', borderRadius: 5 }}>
              <Text style={{ alignSelf: 'center', fontSize: 16, color: '#fff' }}>Download</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={cleardata} style={{ width: 100, height: 40, backgroundColor: colors?.blue, justifyContent: 'center', borderRadius: 5 }}>
              <Text style={{ alignSelf: 'center', fontSize: 16, color: '#fff' }}>Clear</Text>
            </TouchableOpacity>

          </View>

          {/* item added in modal */}
          {showLoader && <Loader />}

        </View>
      </Modal>

      {/* modal */}

      {/* modal part ??????????? start for from date */}
      <Modal isVisible={isModalVisiblefrom}>
        <View>

          <Calendar
            style={{
              borderColor: 'gray',
              height: 370,
              marginTop: 20,
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              borderTopStartRadius: 20,
              borderTopEndRadius: 20,
              width: '90%',
              alignSelf: "center",
              elevation: 6,
              margin: 10,
              shadowOffset: { width: 1, height: 1 },
              shadowColor: '#566573',
              shadowOpacity: 1.0
            }}
            hideExtraDays={true}
            onDayPress={day => {

              setSelected(day.dateString, toggleModalfrom());
            }}
            markedDates={{
              [selected]: { selected: true, disableTouchEvent: true, selectedDotColor: 'orange', }
            }}
          />
        </View>
      </Modal>
      {/* modal for logout */}

      <View>
        <Modal isVisible={isModaldownloadpress}>
          <View style={{ width: "80%", height: 160, backgroundColor: '#fff', alignSelf: 'center', borderRadius: 10, justifyContent: 'center' }}>
            <Text style={{ alignSelf: 'center', fontSize: 18, color: '#000' }}>Are you sure to download ?</Text>

            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 25 }}>
              <View style={{ width: 100, height: 40, backgroundColor: colors?.blue, justifyContent: 'center', borderRadius: 12 }}>
                <TouchableOpacity onPress={downlodpress}>
                  <Text style={{ alignSelf: "center", color: "#fff", fontSize: 16, }}>Download</Text>
                </TouchableOpacity>
              </View>
              <View style={{ width: 100, height: 40, backgroundColor: colors?.red, justifyContent: 'center', borderRadius: 12 }}>
                <TouchableOpacity onPress={toggleModaldownloadpress}>
                  <Text style={{ alignSelf: "center", color: "#fff", fontSize: 16, }}>Cancel</Text>
                </TouchableOpacity>
              </View>
            </View>

          </View>

        </Modal>
      </View>
      {/* modal for logout */}

      {/* modal part  to calender*/}

      <Modal isVisible={isModalVisibleto}>
        <View>

          <Calendar
            style={{
              borderColor: 'gray',
              height: 370,
              marginTop: 20,
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              borderTopStartRadius: 20,
              borderTopEndRadius: 20,
              width: '90%',
              alignSelf: "center",
              elevation: 6,
              margin: 10,
              shadowOffset: { width: 1, height: 1 },
              shadowColor: '#566573',
              shadowOpacity: 1.0
            }}
            hideExtraDays={true}
            onDayPress={day => {

              setSelectedto(day.dateString, toggleModalto());
            }}
            markedDates={{
              [selectedto]: { selected: true, disableTouchEvent: true, selectedDotColor: 'orange', }
            }}
          />
        </View>
      </Modal>



      {/* modal part  to calender end*/}


      <View style={styles.container}>
        <View style={styles.searchroot}>
          <Image
            style={styles.search}
            source={require('../../../assets/search.png')}
          />

          <TextInput
            style={styles.input}
            onChangeText={(text) => searchFilterFunction(text)}
            value={search}
            placeholder="Search"

          />
        </View>
        <FlatList
          data={filteredDataSource}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          horizontal={false} // Change to true for horizontal scrolling
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={handleRefresh}
            />
          }
          ListEmptyComponent={() => <Text style={{ alignSelf: "center" }}>No Data found</Text>}

        />
        {showLoader && <Loader />}

      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    marginBottom: 40,
    marginTop: -10
  },
  card: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    padding: 8,
    marginBottom: 8,
    elevation: 5,
    width: '98%',
    alignSelf: "center",
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,
  },
  image: {
    width: 80,
    height: 80,
    borderRadius: 8,
    marginRight: 8,
    resizeMode: 'center'
  },
  textContainer: {
    flex: 1,
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 2,
    marginHorizontal: 20
  },
  infoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 2,
    marginHorizontal: 20
  },

  icon: {
    marginRight: 4,
  },
  iconfilter: {
    marginRight: 4,
    alignSelf: 'center',
    marginHorizontal: 10
  },
  category: {
    fontSize: 14,
    color: 'gray',
    marginHorizontal: 10
  },
  airline: {
    fontSize: 14,
    color: 'gray',
    marginHorizontal: 10
  },
  flightNumber: {
    fontSize: 14,
    color: 'gray',
    marginHorizontal: 10
  },
  flightNumber1: {
    fontSize: 14,
    color: 'blue',
    marginHorizontal: 10,
    fontWeight: '500'
  },
  status: {
    fontSize: 14,
    color: 'gray',
    marginHorizontal: 10
  },
  date: {
    fontSize: 14,
    color: 'gray',
    marginHorizontal: 10
  },
  headerroot: {
    width: '100%',
    height: Platform.OS == "android" ? 60 : 100,
    backgroundColor: colors?.blue,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    justifyContent: 'center'
  },

  headertext: {
    fontSize: 18,
    alignSelf: "center",
    marginTop: Platform.OS == 'android' ? 10 : 40,
    color: "#fff",
    fontWeight: 'bold',
    letterSpacing: 1
  },

  // filter modal style>>>>>>>
  filtermodalroot: {
    width: 320,
    height: 420,
    backgroundColor: '#fff',
    alignSelf: 'center',
    borderRadius: 10,
    elevation: 10,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,
  },
  modaltoptext: {
    flexDirection: 'row',
    justifyContent: "space-between",
    padding: 15
  },
  filteryourdatatext: {
    fontSize: 16,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: 'blue'
  },
  canbtntext: {
    fontSize: 16,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: 'red'
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: 'blue',
    opacity: 0.4
  },
  calimg: {
    width: 20,
    height: 20,
    resizeMode: 'contain'
  },
  phoneinput: {
    height: 50,
    paddingHorizontal: 12,
    width: "92%",
    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    marginTop: -5,
    borderWidth: 1,
    borderColor: colors?.blue,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,

  },
  inputStyle: { fontSize: 16, margin: -10, marginLeft: 0 },
  labelStyle: { fontSize: 16, color: '#000', marginTop: -5 },
  placeholderStyle: { fontSize: 16 },
  textErrorStyle: {
    fontSize: 16
  },

  // drop down


  dropdown: {
    margin: 16,
    height: 50,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    borderWidth: 1,
    borderWidth: 1,
    borderColor: colors?.blue,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,
  },
  icon: {
    marginRight: 5,
  },
  item: {
    padding: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  placeholderStyle: {
    fontSize: 16,
  },
  selectedTextStyle: {
    fontSize: 16,
  },
  filterbtnroot: {
    flexDirection: 'row',
    width: 150,
    height: 40,
    borderWidth: 2,
    justifyContent: 'center',
    borderRadius: 10,
    borderColor: colors?.blue,
    marginTop: 10,
    marginBottom: 5
  },
  filtertext: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: colors?.blue
  },
  downloadbtnroot: {
    flexDirection: 'row',
    width: 150,
    height: 40,
    borderWidth: 2,
    justifyContent: 'center',
    borderRadius: 10,
    borderColor: colors?.blue,
    marginTop: 10,
  },
  downloadtextroot: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: colors?.blue
  },
  modalroot: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginHorizontal: 18
  },
  datetextroot: {
    width: 130,
    height: 45,
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 5,
    justifyContent: 'center'
  },
  fromdatetext: {
    fontSize: 16,
    fontWeight: "500",
    color: '#000',
    marginHorizontal: 5
  },
  todateroot: {
    width: 130,
    height: 45,
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 5,
    justifyContent: 'center'
  },
  todatetext: {
    fontSize: 16,
    fontWeight: "500",
    color: '#000',
    marginHorizontal: 5
  },
  input: {
    height: 50,
    margin: 12,
    // borderWidth: 1,
    // padding: 10,
    width: '70%',
    fontWeight: '400',
    fontSize: 20, marginTop: -1
  },
  search: {
    width: 25, height: 25,
    resizeMode: 'contain',
    marginTop: 10, margin: 10
  },
  searchroot: {
    flexDirection: "row",
    borderWidth: 1,
    width: '98%',
    alignSelf: 'center',
    // marginTop: 20,
    height: 50,
    borderRadius: 10,
    backgroundColor: '#fff',
    borderColor: colors?.blue,
    elevation: 6,
    marginBottom: 10,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,
  },



});

export default Item;

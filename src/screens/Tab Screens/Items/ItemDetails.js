
import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Image, ScrollView, TouchableOpacity, Keyboard, Button, Platform } from 'react-native';
import { Dropdown } from 'react-native-element-dropdown';
import AntDesign from 'react-native-vector-icons/AntDesign';
import colors from '../../../components/colors';
import { TextInput } from 'react-native-element-textinput';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import Ionicons from 'react-native-vector-icons/Ionicons'; // Use appropriate icon library
import { API_BASE_URL } from '../../../ApiBaseSetUp';
import { Loader } from '../../../components/Loader';
import Entypo
  from 'react-native-vector-icons/Entypo';
  import Toast from 'react-native-simple-toast';



const ItemDetails = (props) => {
  const [showLoader, setShowLoader] = useState(true);
  const [itemliststore, setItemliststore] = useState([]);
  const [imgarr, setImgarr] = useState([]);
  const [status, setStatus] = useState("");
  const [baseimg, setBaseImg] = useState("");



  const [image, setImage] = useState("https://i.postimg.cc/4yZt5wq8/upload-file.png");
  const [image1, setImage1] = useState("https://i.postimg.cc/4yZt5wq8/upload-file.png");
  const [value2, setValue2] = useState("");
  const [value3, setValue3] = useState("");

  const [itemname, setItemname] = useState("");
  const [catnam, setCatnam] = useState("");
  const [airlinenam, setAirlinenam] = useState("");
  const [itemstatus, setItemstatus] = useState("");
  const [flightnum, setFlightnum] = useState("");
  const [seatno, setSeatno] = useState("");
  const [standno, setStandNo] = useState("");
  const [des, setDes] = useState("");
  const [remark, setRemark] = useState("");
  const [uploadedby, setUploadedby] = useState("");
  const [selectedImageId, setSelectedImageId] = useState(null);


  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = (index) => {
    setModalVisible(!isModalVisible);
    setSelectedImageId(index);

  };

  React.useEffect(() => {
    const focusHandler = props.navigation.addListener('focus', () => {
      detailsFetch()
    });
    return focusHandler;
  }, [props]);

  const detailsFetch = async () => {
    setShowLoader(true)

    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/items/details', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },
      body: JSON.stringify({
        item_id: props?.route?.params?.id

      }),

    })
      .then(response => response.json())
      .then(res => {
        setBaseImg(res?.data?.image_base_url)
        setImgarr(res?.data)
        setShowLoader(false)

        res?.data?.airlines.map((indx) => {
          setValue3(indx?.id)

        })
        res?.data?.items.map((indx) => {
          setStatus(indx?.item_status)
          setItemname(indx?.item_name)
          setCatnam(indx?.category_name)
          setAirlinenam(indx?.airline_name)
          setItemstatus(indx?.item_status)
          setFlightnum(indx?.flight_no)
          setSeatno(indx?.seat_no)
          setStandNo(indx?.stand_no)
          setDes(indx?.item_description)
          setRemark(indx?.remark)
          setUploadedby(indx?.uploded_by_name)


        })
      })
      .catch(error => {
        Toast.show(
          error.message || 'An error occurred',
          Toast.BOTTOM, 
        );
        setShowLoader(false);
      });
  };


  return (

    <>
      <View style={styles.headerroot}>

        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Ionicons name="arrow-back" size={26} color={"#fff"} style={styles.arrback} />
        </TouchableOpacity>
        <Text style={styles.headertext}>{itemname}</Text>

      </View>
      <ScrollView>
        <View style={styles.cardroot}>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Item Name:</Text>
            <Text style={styles.values}>{itemname == "" ? <Text>No data found</Text> : itemname}</Text>
          </View>
          <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
          </View>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Category Name:</Text>
            <Text style={styles.values}>{catnam == "" ? <Text>No data found</Text> : catnam}</Text>
          </View>
          <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
          </View>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Airline Name:</Text>
            <Text style={styles.values}>{airlinenam == "" ? <Text>No data found</Text> : airlinenam}</Text>
          </View>
          <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
          </View>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Item Status:</Text>
            <Text style={styles.values}>{itemstatus == "" ? <Text>No data found</Text> : itemstatus}</Text>
          </View>
          <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
          </View>
          {showLoader && <Loader />}
          <View>
          </View>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Flight Number:</Text>
            <Text style={styles.values}>{flightnum == "" ? <Text>No data found</Text> : flightnum}</Text>
          </View>
          <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
          </View>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Seat Number:</Text>
            <Text style={styles.values}>{seatno == "" ? <Text>No data found</Text> : seatno}</Text>
          </View>
          <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
          </View>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Stand Number:</Text>
            <Text style={styles.values}>{standno == "" ? <Text>No data found</Text> : standno}</Text>
          </View>
          <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
          </View>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Descriptions:</Text>
            <Text style={styles.values}>{des == "" ? <Text>No data found</Text> : des}</Text>
          </View>
          <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: des?.length >= 40 ? 60 : 12 }}>
          </View>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Remark:</Text>
            <Text style={styles.values}>{remark == "" ? <Text>No data found</Text> : remark}</Text>
          </View>
          <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
          </View>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10, color: '#000' }}>Item Uploaded By:</Text>
            <Text style={styles.values}>{uploadedby == "" ? <Text>No data found</Text> : uploadedby}</Text>
          </View>
          <View style={{ width: '100%', height: 1, backgroundColor: 'blue', opacity: 0.3, marginTop: 12 }}>
          </View>
          <ScrollView horizontal={true} style={{ alignSelf: 'center' }}>
            {

              imgarr?.images?.map((image) => {
                const selectedImage = imgarr?.images?.find((image) => image.id === selectedImageId);

                return (
                  <View style={{
                  }}>
                    <Text style={{ alignSelf: "center", color: colors?.blue, fontWeight: 'bold', marginTop: 10, fontSize: 10, margin: 5, marginHorizontal: 20, marginBottom: 5 }}>Uploaded Images</Text>
                    <TouchableOpacity key={image.id} onPress={() => toggleModal(image?.id)}>
                      <Image
                        key={image}
                        style={styles.logo}
                        source={{
                          uri: baseimg + "/" + image?.image_name
                        }}
                      />
                    </TouchableOpacity>
                    {/* modal for logout */}

                    <View >
                      <Modal isVisible={isModalVisible}>
                        <View style={{ alignSelf: 'center' }}>
                          <TouchableOpacity onPress={toggleModal}>
                            <Entypo style={{ alignSelf: 'flex-end' }} color={colors?.red} name="circle-with-cross" size={40} />
                          </TouchableOpacity>
                          <Image
                            source={{ uri: baseimg + "/" + selectedImage?.image_name }}
                            style={{ width: 350, height: 400, resizeMode: 'contain', }}
                          />
                        </View>

                      </Modal>
                    </View>
                    {/* modal for logout */}
                  </View>

                )
              })

            }
          </ScrollView>


        </View>

      </ScrollView>

      <View >
        {
          status == "Handed over" ? null :
            <TouchableOpacity onPress={() => props?.navigation.navigate("ItemHandOver", {
              id: props?.route?.params?.id
            })} style={styles.submitbtnroot}>
              <Text style={{ alignSelf: "center", fontSize: 16, color: "#fff", fontWeight: 'bold', letterSpacing: 1 }}>Handover Item</Text>
            </TouchableOpacity>
        }


      </View>



    </>

  );
};

export default ItemDetails;

const styles = StyleSheet.create({
  headerroot: {
    width: '100%',
    height: Platform.OS == "android" ? 60 : 100,
    backgroundColor: colors?.blue,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    flexDirection: 'row',
  },

  headertext: {
    fontSize: 18,
    alignSelf: "center",
    color: "#fff",
    fontWeight: 'bold',
    letterSpacing: 1,
    marginLeft: "10%",
    marginTop: Platform.OS == 'android' ? 10 : 40,

    // marginLeft: '20%'
  },

  submitbtnroot: {
    width: '95%',
    height: 50,
    backgroundColor: 'green',
    justifyContent: "center",
    borderRadius: 8,
    alignSelf: 'center',
    marginBottom: 5
  },
  cardroot: {
    width: '95%',
    height: 700,
    alignSelf: 'center',
    marginTop: 10,
    borderRadius: 12,
    backgroundColor: '#fff',
    elevation: 5,
    marginBottom: 10,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,
  },
  values: {
    position: 'absolute',
    alignSelf: "flex-end",
    marginLeft: '30%',
    fontSize: 14,
    fontWeight: 'bold',
    color: 'green', margin: 10
  }
  ,
  arrback: {
    alignSelf: "center",
    marginLeft: 10,
    marginTop: Platform.OS == 'android' ? 16 : 60,
  },
  logo: {
    resizeMode: 'contain',
    width: "80%", height: 80,
    borderRadius: 5,
    alignSelf: "center"
  },

});

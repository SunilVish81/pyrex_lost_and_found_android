import {
  Image,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Platform
} from 'react-native';
import React, { useState, useEffect } from 'react';
const { height, width } = Dimensions.get('window');
import Fontisto from 'react-native-vector-icons/Fontisto';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons
  from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../components/colors';
import { API_BASE_URL } from '../../ApiBaseSetUp';
import { Loader } from '../../components/Loader';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from "react-native-modal";
import Toast from 'react-native-simple-toast';


const Dashboard = props => {
  const [showLoader, setShowLoader] = useState(false);
  const [data, setData] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [isModalVisible, setModalVisible] = useState(false);
  const [isModalVisible1, setModalVisible1] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  const toggleModal1 = () => {
    setModalVisible1(!isModalVisible1);
  };

  React.useEffect(() => {
    const focusHandler = props.navigation.addListener('focus', () => {
      Getdasdata()
    });
    return focusHandler;
  }, [props]);
  

  const deleteuser = async () => {

    setShowLoader(true);

    const Token = await AsyncStorage.getItem('token')
    setShowLoader(true);
    await fetch(API_BASE_URL + 'user/delete/account', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,
      },
    })
      .then(response => response.json())
      .then(res => {
        if (res?.success == true) {
          alert(res?.message)
          toggleModal1()
          clearstorage()
        }
        setShowLoader(false)
      })
      .catch(error => {
        Toast.show(
          error.message || 'An error occurred',
          Toast.BOTTOM,

        );
        setShowLoader(false);
      });
  };



  const Getdasdata = async () => {
    setShowLoader(true);

    const Token = await AsyncStorage.getItem('token')
    const name = await AsyncStorage.getItem('name')
    setName(name)
    const email = await AsyncStorage.getItem('email')
    setEmail(email)
    setShowLoader(true);
    await fetch(API_BASE_URL + 'user/get/dashboard/statistics', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },

    })
      .then(response => response.json())
      .then(res => {
        setData(res?.data)
        setShowLoader(false)
      })
      .catch(error => {
        Toast.show(error?.message, Toast.BOTTOM);

        setShowLoader(false);
      });
  };

  const clearstorage = () => {
    AsyncStorage.clear()
    toggleModal()
    props.navigation.navigate('Login')
  }


  return (
    <View style={styles.container}>

      <View style={styles.logoroot}>
        <View>
          <Image
            source={require('../../assets/BrandLogo.png')}
            style={styles.tophead}
          />
          {
            Platform.OS == "android" ?
              <TouchableOpacity onPress={toggleModal}>
                <Image
                  source={require('../../assets/power-off.png')}
                  style={{ position: 'absolute', marginTop: -30, resizeMode: 'contain', width: 30, height: 30, alignSelf: 'flex-end', marginLeft: '2%' }}
                />
              </TouchableOpacity>
              :

              <TouchableOpacity onPress={toggleModal}>
                <Image
                  source={require('../../assets/swi.png')}
                  style={{ alignSelf: 'flex-end', width: 30, height: 30, marginTop: -25, left: -4 }}
                />
              </TouchableOpacity>
          }
        </View>
        <Text style={{ fontSize: 18, alignSelf: "center", marginTop: Platform.OS == "android" ? 10 : 2, color: "#fff" }}>Pyrex Lost & Found</Text>
      </View>

      <>
        <View style={{ flexDirection: 'row', marginHorizontal: 10, marginTop: 10, marginLeft: 20 }}>
          <Fontisto style={{ alignSelf: "center" }} color={colors?.blue} name="person" size={12} />
          <Text style={{ marginHorizontal: 10, fontSize: 12, color: "#000", opacity: 0.8, fontWeight: '400', }}>{name}</Text>
        </View>
        {/* delete account */}
        <TouchableOpacity onPress={toggleModal1}>
          <View>
            <Text style={{ alignSelf: "flex-end", right: 5, position: 'absolute', color: "red", fontSize: 10, textDecorationLine: 'underline' }}>Delete your account</Text>
          </View>
        </TouchableOpacity>
        {/* delete account */}

        <View style={{ flexDirection: 'row', marginHorizontal: 10, marginTop: 5, marginLeft: 20 }}>
          <MaterialCommunityIcons style={{ alignSelf: "center" }} color={colors?.blue} name="email" size={12} />
          <Text style={{ marginHorizontal: 10, fontSize: 12, color: "#000", fontWeight: '400', opacity: 0.8, }}>{email}</Text>
        </View>



        <View style={styles.allocationView}>

          <View style={styles.listContainer}>
            <View style={styles.listBlock}>
              <View style={styles.listIconBlock}>
                <Image
                  source={require('../../assets/to-do.png')}
                  style={styles.itemsimg}
                />
                <Text style={styles.timeTextforhead}>Total Items</Text>
              </View>
              <View>
                <Text style={styles.timeText}>{data?.total_items == "" ? <Text>0</Text> : data?.total_items}</Text>

              </View>
            </View>
            <View style={styles.listBlock}>
              <View
                style={[
                  styles.listIconBlock,
                  {
                    width: width * 0.5,
                  },
                ]}>
                <Image
                  source={require('../../assets/process.png')}
                  style={styles.itemsimg}
                />

                <Text style={styles.timeTextforhead}>Total In Possession</Text>
              </View>
              <View>
                <Text style={styles.timeText}>{data?.total_inpossession == "" ? <Text>0</Text> : data?.total_inpossession}</Text>
              </View>
            </View>
            <View style={styles.listBlock}>
              <View style={styles.listIconBlock}>
                <Image
                  source={require('../../assets/giving.png')}
                  style={styles.itemsimg}
                />
                <Text style={styles.timeTextforhead}>Total Handovered Items</Text>
              </View>

              <Text style={styles.timeText}>{data?.total_handovered_items == "" ? <Text>0</Text> : data?.total_handovered_items}</Text>

              {showLoader && <Loader />}

            </View>

            <View style={styles.listBlock}>
              <View
                style={[
                  styles.listIconBlock,
                  {
                    width: width * 0.5,
                  },
                ]}>
                <Image
                  source={require('../../assets/garbage.png')}
                  style={styles.itemsimg}
                />

                <Text style={styles.timeTextforhead}>Total Disposed</Text>
              </View>
              <View>
                <Text style={styles.timeText}>{data?.total_disposed_off == "" ? <Text>0</Text> : data?.total_disposed_off}</Text>
              </View>
            </View>
            <View style={styles.listBlock}>
              <View
                style={[
                  styles.listIconBlock,
                  {
                    width: width * 0.5,
                  },
                ]}>
                <Image
                  source={require('../../assets/other.png')}
                  style={styles.itemsimg}

                />

                <Text style={styles.timeTextforhead}> Total Others</Text>
              </View>
              <View>
                <Text style={styles.timeText}>{data?.total_others
                  == "" ? <Text>0</Text> : data?.total_others
                }</Text>
              </View>
            </View>
          </View>
        </View>
      </>
      {/* modal for logout */}

      <View style={{ flex: 1 }}>
        <Modal isVisible={isModalVisible}>
          <View style={styles.modalrootcard}>
            <Text style={styles.areyousuretext}>Are You sure to logout ?</Text>
            <View style={styles.btndirroot}>
              <TouchableOpacity onPress={clearstorage} style={styles.logoutroot}>
                <Text style={styles.logouttext}>Logout</Text>

              </TouchableOpacity>
              <TouchableOpacity onPress={toggleModal} style={styles.canbtnroot}>
                <Text style={styles.logouttext}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
      {/* modal for logout */}
      {/* modal for logout */}

      <View style={{ flex: 1 }}>
        <Modal isVisible={isModalVisible1}>
          <View style={styles.modalrootcard}>
            <Text style={styles.areyousuretext}>Are you sure to delete your account ?</Text>
            <View style={styles.btndirroot}>
              <TouchableOpacity onPress={deleteuser} style={styles.logoutroot}>
                <Text style={styles.logouttext}>Yes</Text>

              </TouchableOpacity>
              <TouchableOpacity onPress={toggleModal1} style={styles.canbtnroot}>
                <Text style={styles.logouttext}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
      {/* modal for logout */}

    </View>

  );
};

export default Dashboard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  logoBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: height * 0.01,
    marginHorizontal: width * 0.05,
  },
  logoImg: {
    width: width * 0.5,
    height: height * 0.08,
    resizeMode: 'contain'
    // marginRight: width * 0.05,
  },
  logoroot: {
    width: '100%',
    height: Platform.OS == "android" ? 110 : 150,
    backgroundColor: colors?.blue,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    justifyContent: 'center'
  },
  dateText: {
    color: colors.Gray,
    fontSize: 15,
  },
  timeText: {
    color: colors.black,
    fontSize: 15,
    // fontFamily: "Roboto-Medium",
    letterSpacing: 1,
  },
  timeTextforhead: {
    color: colors.blue,
    fontSize: 14,
    /// fontFamily: "Roboto-Medium",
    letterSpacing: 1,
    width: 200,
    alignSelf: "center",
    marginLeft: 10,
    fontWeight: 'bold',
    letterSpacing: 1
  },
  timeBlock: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  allocationView: {
    flexDirection: 'row',
    justifyContent: 'space-between',

    alignSelf: "center"
  },
  allocationBlock: {
    width: width * 0.4,
    height: height * 0.1,
    backgroundColor: '#fff',
    borderRadius: 12,
    borderWidth: width * 0.00001,
    borderBottomWidth: 8,
    padding: width * 0.03,
    borderBottomColor: 'blue',
    flexDirection: 'row',
    elevation: 5,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,

  },
  allocationChild: {
    marginLeft: width * 0.02,
  },
  listBlock: {
    width: width * 0.9,
    height: height * 0.1,
    backgroundColor: colors.White,
    borderWidth: 0.6,

    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: width * 0.02,
    marginTop: 10,
    backgroundColor: '#fff',
    elevation: 4,
    borderRadius: 6,
    borderColor: colors?.blue,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,

  },
  listContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: height * 0.01,
    marginHorizontal: width * 0.05,
  },
  listIconBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: width * 0.35,
  },
  noteText: {
    fontSize: 12.5,
    color: colors.Gray,
    fontWeight: '500',
  },
  noteContainer: {
    flexDirection: 'row',
    marginVertical: height * 0.01,
    marginHorizontal: width * 0.05,
    justifyContent: 'space-around',
  },
  syncContainer: {
    flexDirection: 'row',
    marginVertical: height * 0.01,
    marginHorizontal: width * 0.05,
    position: 'absolute',
    bottom: height * 0.1,
  },
  syncBlock: {
    width: width * 0.65,
    height: height * 0.08,
    backgroundColor: '#99e699',
    borderTopLeftRadius: 12,
    borderBottomLeftRadius: 12,
    justifyContent: 'center',
  },
  syncText: {
    color: colors.White,
    fontWeight: '800',
    fontSize: 15,
    marginLeft: width * 0.1,
  },
  syncBlock_2: {
    width: width * 0.25,
    height: height * 0.08,
    backgroundColor: 'white',
    borderTopRightRadius: 12,
    borderBottomRightRadius: 12,
    borderColor: '#99e699',
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemsimg: {
    resizeMode: 'contain',
    height: 30,
    width: 30
  },
  tophead: {
    resizeMode: "contain",
    height: 55,
    width: 55,
    alignSelf: "center",
    marginTop: Platform.OS == "android" ? 0 : 50
  },
  modalrootcard: {
    width: '80%',
    height: 180,
    backgroundColor: '#fff',
    alignSelf: "center",
    borderRadius: 10,
    justifyContent: 'center'
  },
  areyousuretext: {
    alignSelf: "center",
    fontSize: 18,
    fontWeight: 'bold',
    color: colors?.blue
  },
  btndirroot: {
    flexDirection: 'row',
    justifyContent: "space-around",
    marginTop: 40
  },
  logoutroot: {
    width: 100,
    height: 40,
    backgroundColor: 'red',
    justifyContent: 'center',
    borderRadius: 10,
  }, logouttext: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#fff',
    fontWeight: 'bold'
  },
  canbtnroot: {
    width: 100,
    height: 40,
    backgroundColor: colors?.blue,
    justifyContent: 'center',
    borderRadius: 10,
  }

});
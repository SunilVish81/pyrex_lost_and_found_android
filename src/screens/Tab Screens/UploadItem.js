import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Platform, Keyboard, Alert } from 'react-native'
import React, { useState, useEffect } from 'react'
import colors from '../../components/colors'
import { Dropdown } from 'react-native-element-dropdown';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { TextInput } from 'react-native-element-textinput';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-crop-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { API_BASE_URL } from '../../ApiBaseSetUp';
import { Loader } from '../../components/Loader';
import Toast from 'react-native-simple-toast';


const UploadItem = (props) => {
  const [value1, setValue1] = useState("");
  const [value2, setValue2] = useState("");
  const [isModalVisiblebeneficial, setModalVisiblebeneficial] = useState(false);
  const [isModalVisiblebeneficial1, setModalVisiblebeneficial1] = useState(false);
  const [flightnumber, setFlightNumber] = useState("");
  const [seatnumber, setSeatnumber] = useState("");
  const [standnumber, setStandNumber] = useState("");
  const [itemname, setItemname] = useState("");
  const [description, setDescription] = useState("");
  const [anyremark, setAnyremark] = useState("");
  const [showLoader, setShowLoader] = useState(false);

  const [image, setImage] = useState("https://i.postimg.cc/4yZt5wq8/upload-file.png");
  const [image1, setImage1] = useState("https://i.postimg.cc/4yZt5wq8/upload-file.png");
  const [conditionsrender, setConditionsrender] = useState(null);
  const [storeImgBase1, setStoreImgBase1] = useState([]);
  const [storeImgBase2, setStoreImgBase2] = useState([]);
  const [storestate, setStoreState] = useState([]);
  const [openKey, setOpenKey] = useState("");



  const [catdata, setCatdata] = useState([]);
  const [airname, setAirname] = useState([]);

  const ModalOpen = () => {
    setModalVisiblebeneficial(!isModalVisiblebeneficial)

  }
  const ModalOpen1 = () => {
    setModalVisiblebeneficial1(!isModalVisiblebeneficial1)

  }

  // clear states >>>>>>>>>>>>>>>>>>>>>>
  React.useEffect(() => {
    const focusHandler = props.navigation.addListener('focus', () => {
      setValue1("")
      setValue2("")
      setFlightNumber("")
      setSeatnumber("")
      setStandNumber("")
      setStandNumber("")
      setItemname("")
      setDescription("")
      setAnyremark("")
      // setStoreImgBase1("")
      // setStoreImgBase2("")
      // setImage("https://i.postimg.cc/4yZt5wq8/upload-file.png")
    });
    return focusHandler;
  }, [props]);

  // Add an event listener for keyboard open event


  const Pifromcamimg1 = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      multiple: true,
      includeBase64: true,
    }).then(image => {
      setImage(image?.path)
      Toast.show("Images Uploaded", Toast.BOTTOM);
      const formattedData = [];
      formattedData.push(`data:${image.mime};base64,${image.data}`)
      setStoreImgBase2(formattedData)
      ModalOpen1()

    });
  }

  const PifromGalimg1 = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      multiple: true,
      includeBase64: true,
    }).then(image => {
      setConditionsrender(image)
      setImage1(image?.path)
      Toast.show("Images Uploaded", Toast.BOTTOM);
      const formattedData = [];
      image?.map((indx) => {
        formattedData.push(`data:${indx.mime};base64,${indx.data}`)
      })
      setStoreImgBase1(formattedData)

      ModalOpen()
    });
  };

  const renderItem1 = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {item.value1 === value1 && (
          <AntDesign
            style={styles.icon}
            color="black"
            name="Safety"
            size={20}
          />
        )}
      </View>
    );
  };

  const renderItem2 = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {item.value2 === value2 && (
          <AntDesign
            style={styles.icon}
            color="black"
            name="Safety"
            size={20}
          />
        )}
      </View>
    );
  };

  React.useEffect(() => {
    const focusHandler = props.navigation.addListener('focus', () => {
      Getdasdata()
      airnameget()
    });
    return focusHandler;
  }, [props]);


  const Getdasdata = async () => {

    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/item/categories', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },
    })
      .then(response => response.json())
      .then(res => {
        setCatdata(res?.data)
      })
      .catch(error => {
        Toast.show(error?.message, Toast.BOTTOM);
        setShowLoader(false);
      });
  };

  const airnameget = async () => {

    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/airlines', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },

    })
      .then(response => response.json())
      .then(res => {
        setAirname(res?.data)

      })
      .catch(error => {
        Toast.show(error?.message, Toast.BOTTOM);
        setShowLoader(false);
      });
  };

  const airrr = airname?.map(item => ({
    label: item?.airline_name,
    value: item?.id
  }));


  const categoryy = catdata?.map(item => ({
    label: item?.category_name,
    value: item?.id
  }));


  const OnSubmit = async () => {
    const Concats = storeImgBase1.concat(storeImgBase2)
    const Token = await AsyncStorage.getItem('token')

    if (value1 == "") {
      Toast.show("Please select airline name!", Toast.BOTTOM);
      return;
    }
    if (flightnumber == "") {
      Toast.show('Please enter flight number!', Toast.BOTTOM);
      return;
    }
    if (seatnumber == "") {
      Toast.show('Please enter seat number!', Toast.BOTTOM);
      return;
    }

    if (value2 == "") {
      Toast.show('Please select category!', Toast.BOTTOM);
      return;
    }
    if (itemname == "") {
      Toast.show('Please enter item name!', Toast.BOTTOM);
      return;
    }
    if (description == "") {
      Toast.show('Please enter description!', Toast.BOTTOM);
      return;
    }
    if (Concats.length <= 0) {
      Toast.show('Image fields is required', Toast.BOTTOM);
      return;
    }

    setShowLoader(true);
    await fetch(API_BASE_URL + 'user/upload/item', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },
      body: JSON.stringify({
        item_name: itemname,
        item_description: description,
        category_id: value2,
        airline_id: value1,
        flight_no: flightnumber,
        seat_no: seatnumber,
        stand_no: standnumber,
        remark: anyremark,
        item_first_images: Concats,
        // item_second_images: storeImgBase2,
      }),

    })
      .then(response => response.json())
      .then(res => {
        setShowLoader(false);
        if (res?.success === true) {
          setImage("https://i.postimg.cc/4yZt5wq8/upload-file.png")
          props.navigation.navigate('Item')
          Toast.show(res?.message, Toast.BOTTOM);
        } else if (res?.success === false) {
          Toast.show(res?.message, Toast.BOTTOM);
          setShowLoader(false);
        }
      })
      .catch(error => {
        Toast.show('Item Uploaded Successfully', Toast.BOTTOM);
        props.navigation.navigate('Item')
        setShowLoader(false);
      });
  };
  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => {
      setOpenKey(1)
    });

    const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setOpenKey(2)
    });

    return () => {
      keyboardDidShowListener.remove();
      keyboardDidHideListener.remove();
    };
  }, []);
  
  return (
    <>

      <View style={styles.headerroot}>
        <Text style={styles.headertext}>Upload Item</Text>
      </View>
      <View>

        <ScrollView style={{ marginBottom: Platform.OS == "ios" && openKey == 1 ? 320 : 50 }}>

          <View style={styles.cardfirst}>
            <View>
              <Text style={styles.uploadetailstext}>Uploads Details</Text>
            </View>
            <View style={styles.line}>

            </View>
            <Dropdown
              style={styles.dropdown}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              containerStyle={{ backgroundColor: "#EBEDEF" }}
              iconStyle={styles.iconStyle}
              data={airrr}
              search
              maxHeight={300}
              labelField="label"
              valueField="value"
              placeholder="Select Airline Name"
              searchPlaceholder="Search..."
              value={value1}
              onChange={item => {
                setValue1(item.value);
              }}

              renderItem={renderItem1}
            />
            <View>
              <View style={{ flexDirection: "row", alignSelf: 'center', marginTop: -15 }}>
                <View style={{ width: "48%" }}>
                  <TextInput
                    value={flightnumber}
                    autoCapitalize={"characters"}
                    style={styles.phoneinput}
                    inputStyle={styles.inputStyle}
                    labelStyle={styles.labelStyle}
                    placeholderStyle={styles.placeholderStyle}
                    textErrorStyle={styles.textErrorStyle}
                    label="Flight Number"
                    placeholder="Enter Flight Number"
                    placeholderTextColor="gray"
                    onChangeText={text => {
                      setFlightNumber(text);
                    }}
                  />
                </View>
                <View style={{ width: "48%" }}>

                  <TextInput
                    value={seatnumber}
                    autoCapitalize={"characters"}
                    style={styles.phoneinput}
                    inputStyle={styles.inputStyle}
                    labelStyle={styles.labelStyle}
                    placeholderStyle={styles.placeholderStyle}
                    textErrorStyle={styles.textErrorStyle}
                    label="Seat Number"
                    placeholder="Enter Seat Number"
                    placeholderTextColor="gray"
                    onChangeText={text => {
                      setSeatnumber(text);
                    }}
                  />
                </View>

              </View>
              <TextInput
                value={standnumber}
                style={styles.phoneinput}
                inputStyle={styles.inputStyle}
                labelStyle={styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                label="Stand Number"
                placeholder="Enter Stand Number"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setStandNumber(text);
                }}
              />
              <View style={{ marginTop: -5 }}>
                <Dropdown
                  style={styles.dropdown}
                  placeholderStyle={styles.placeholderStyle}
                  selectedTextStyle={styles.selectedTextStyle}
                  inputSearchStyle={styles.inputSearchStyle}
                  iconStyle={styles.iconStyle}
                  containerStyle={{ backgroundColor: "#EBEDEF" }}
                  data={categoryy}
                  search
                  maxHeight={300}
                  labelField="label"
                  valueField="value"
                  placeholder="Select Category"
                  searchPlaceholder="Search..."
                  value={value2}
                  onChange={item => {
                    setValue2(item.value);
                  }}

                  renderItem={renderItem2}
                />
              </View>

              <View style={{ marginTop: -15 }}>
                <TextInput
                  value={itemname}
                  style={styles.phoneinput}
                  inputStyle={styles.inputStyle}
                  labelStyle={styles.labelStyle}
                  placeholderStyle={styles.placeholderStyle}
                  textErrorStyle={styles.textErrorStyle}
                  label="Item Name"
                  placeholder="Enter Item Name"
                  placeholderTextColor="gray"
                  onChangeText={text => {
                    setItemname(text);
                  }}
                />
              </View>
              {showLoader && <Loader />}


              <TextInput
                value={description}
                style={styles.descriptions}
                inputStyle={styles.inputStyle}
                labelStyle={Platform.OS == "ios" ? styles.labelStyle1 : styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                label="Item Description"
                multiline={true}
                placeholder="Enter Item Description"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setDescription(text);
                }}
              />
              <TextInput
                value={anyremark}
                style={styles.descriptions}
                inputStyle={styles.inputStyle}
                labelStyle={Platform.OS == "ios" ? styles.labelStyle1 : styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                multiline={true}
                label="Any Remarks(Optional)"
                placeholder="Enter any Remarks (Optional)"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setAnyremark(text);
                }}
              />
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>

              <View style={{ width: '40%', height: 100, backgroundColor: '#ffff', margin: 12, borderRadius: 10, elevation: 4, borderWidth: 1, borderColor: colors?.blue }}>
                <Text style={{ alignSelf: "center", color: colors?.blue, fontWeight: 'bold', marginTop: 5, fontSize: 12 }}>Upload by gallery</Text>
                <TouchableOpacity onPress={ModalOpen}>
                  <Image
                    style={styles.logo}
                    source={{
                      uri: "https://i.postimg.cc/4yZt5wq8/upload-file.png",
                    }}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ width: '40%', height: 100, backgroundColor: '#ffff', margin: 12, borderRadius: 10, elevation: 4, borderWidth: 1, borderColor: colors?.blue }}>
                <Text style={{ alignSelf: "center", color: colors?.blue, fontWeight: 'bold', marginTop: 5, fontSize: 12 }}>Upload by Camera</Text>
                <TouchableOpacity onPress={ModalOpen1}>
                  <Image
                    style={styles.logo}
                    source={{
                      uri: image,
                    }}
                  />
                </TouchableOpacity>
              </View>

            </View>

            {/* modal added  */}

            <View>
              <Modal isVisible={isModalVisiblebeneficial}
                animationType="slide"
              >
                <View>

                  <TouchableOpacity onPress={PifromGalimg1}>
                    <View style={styles.choosefromgaltext}>
                      <Text style={styles.modaltext}>Choose From Gallery</Text>

                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={ModalOpen} >
                    <View style={styles.closetextroot}>
                      <Text style={styles.closetext}>Close</Text>

                    </View>
                  </TouchableOpacity>
                </View>
              </Modal>
            </View>


            {/* modal added end  */}
            {/* modal added  */}

            <View>
              <Modal isVisible={isModalVisiblebeneficial1}
                animationType="slide"
              >
                <View>
                  <TouchableOpacity onPress={Pifromcamimg1}>
                    <View style={styles.choosfromtext}>
                      <Text style={styles.modaltext}>Choose From Camera</Text>

                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={ModalOpen1} >
                    <View style={styles.closetextroot}>
                      <Text style={styles.closetext}>Close</Text>

                    </View>
                  </TouchableOpacity>
                </View>
              </Modal>
            </View>


            {/* modal added end  */}


          </View>
          <TouchableOpacity onPress={OnSubmit} style={styles.submitroot}>
            <Text style={styles.uploaditemdetailstext}>Upload Item
              Details</Text>
          </TouchableOpacity>
          {
            Platform.OS == "android" ? null :
              <View style={{ width: 100, height: 50, }}>
              </View>
          }
        </ScrollView>
      </View>
    </>
  )
}

export default UploadItem

const styles = StyleSheet.create({
  headerroot: {
    width: '100%',
    height: Platform.OS == "android" ? 60 : 100,
    backgroundColor: colors?.blue,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    justifyContent: 'center',
  },

  headertext: {
    fontSize: 18,
    alignSelf: "center",
    marginTop: Platform.OS == 'android' ? 10 : 40,
    color: "#fff",
    fontWeight: 'bold',
    letterSpacing: 1
  },
  cardfirst: {
    width: '95%',
    height: 680,
    backgroundColor: "#fff",
    marginTop: 10,
    alignSelf: "center",
    elevation: 4,
    borderRadius: 8,
    marginBottom: 20
  },

  dropdown: {
    margin: 16,
    height: 50,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    borderWidth: 1,
    borderWidth: 1,
    borderColor: colors?.blue,
  },
  icon: {
    marginRight: 5,
  },
  item: {
    padding: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  placeholderStyle: {
    fontSize: 16,
  },
  selectedTextStyle: {
    fontSize: 16,
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 16,
  },
  phoneinput: {
    height: 50,
    paddingHorizontal: 12,
    width: "92%",
    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    marginTop: 12,
    borderWidth: 1,
    borderColor: colors?.blue,

  },
  descriptions: {
    height: 80,
    paddingHorizontal: 12,
    width: "92%",

    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    marginTop: 12,
    borderWidth: 1,
    borderColor: colors?.blue,

  },
  inputStyle: { fontSize: 16, margin: -10, marginLeft: 0 },
  labelStyle: { fontSize: 16, color: colors?.blue, marginTop: -5, fontWeight: 'bold', },
  labelStyle1: { fontSize: 16, color: colors?.blue, marginTop: -5, fontWeight: 'bold', marginBottom: 10 },
  placeholderStyle: { fontSize: 16 },
  textErrorStyle: {
    fontSize: 16
  },
  logo: {
    resizeMode: 'contain',
    width: " 100%", height: 60
  },
  choosfromtext: {
    width: 180,
    height: 40,
    backgroundColor: "green",
    alignSelf: 'center',
    borderRadius: 10,
    justifyContent: 'center'
  },
  choosefromgaltext: {
    width: 180,
    height: 40,
    backgroundColor: colors?.blue,
    alignSelf: 'center',
    marginTop: 10,
    borderRadius: 10,
    justifyContent: 'center'
  },
  modaltext: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 12,
    fontWeight: '500'
  },
  closetextroot: {
    width: 150,
    height: 40,
    backgroundColor: "red",
    alignSelf: 'center',
    marginTop: 10,
    borderRadius: 10,
    justifyContent: 'center'
  },
  closetext: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 12,
    fontWeight: '500'
  },
  uploadetailstext: {
    fontSize: 16,
    color: "#000",
    fontWeight: "bold",
    marginLeft: 22,
    marginTop: 10
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: colors?.blue,
    marginTop: 12,
    opacity: 0.4
  },
  submitroot: {
    width: '95%',
    height: 50,
    backgroundColor: 'green',
    justifyContent: "center",
    borderRadius: 8,
    alignSelf: 'center',
    marginBottom: 80
  },
  uploaditemdetailstext: {
    alignSelf: "center",
    fontSize: 18,
    color: "#fff",
    fontWeight: 'bold',
    letterSpacing: 1
  }
})


import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, View, Text, Image, ScrollView, TouchableOpacity, Keyboard, Platform } from 'react-native';
import { Dropdown } from 'react-native-element-dropdown';
import AntDesign from 'react-native-vector-icons/AntDesign';
import colors from '../../components/colors';
import { TextInput } from 'react-native-element-textinput';
import { Loader } from '../../components/Loader';
import AsyncStorage from '@react-native-community/async-storage';
import { API_BASE_URL } from '../../ApiBaseSetUp';
import SignatureScreen from "react-native-signature-canvas";
import Modal from "react-native-modal";
import Toast from 'react-native-simple-toast';





const ItemHandOver = (props) => {
  const [showLoader, setShowLoader] = useState(false);
  const [scrollEnabled, setScrollEnabled] = useState(true);
  const [isModalVisible, setModalVisible] = useState(false);
  const [signatureID, setSignatureID] = useState("");
  const [staffID, setStaffID] = useState("");
  const [openKey, setOpenKey] = useState("");


  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };


  const [value1, setValue1] = useState("");
  const [value2, setValue2] = useState("");
  const [value3, setValue3] = useState("");


  const [name, setName] = useState("");
  const [contactno, setContactno] = useState("");
  const [email, setEmail] = useState("");
  const [itemliststore, setItemliststore] = useState([]);
  const [handovertolist, setHandovertolist] = useState([]);
  const [stafflistsss, setStafflistsss] = useState([]);
  const [signature, setSignature] = useState("");


  // handover by details

  const [hadnoverbyname, setHandoverbyname] = useState("");
  const [hadnoverbyadress, setHandoverbyaddress] = useState("");
  const [remark, setRemark] = useState("");


  const ref = useRef();

  const handleOK = (signature) => {
    AsyncStorage.setItem("sign", signature)
    toggleModal()
    OnSubmitSignature()
  };
  const handleEmpty = () => {
    console.log("Empty");
  };

  const handleClear = () => {
    console.log("clear success!");
  };

  const handleData = (data) => {
    console.log(data);
  };
  const iditem = props?.route?.params?.id
  const sutoselitem = () => {
    setValue1(iditem)
  }

  React.useEffect(() => {
    const focusHandler = props.navigation.addListener('focus', () => {
      setValue1("")
      setValue2("")
      setValue3("")
      setName("")
      setContactno("")
      setHandoverbyaddress("")
      setHandoverbyname("")
      setStaffID("")
      setRemark("")
      setEmail("")
    });
    return focusHandler;
  }, [props]);

  const renderItem1 = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {item.value1 === value1 && (
          <AntDesign
            style={styles.icon}
            color="black"
            name="Safety"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderItem2 = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {item.value2 === value2 && (
          <AntDesign
            style={styles.icon}
            color="black"
            name="Safety"
            size={20}
          />
        )}
      </View>
    );
  };
  const renderItem3 = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.label}</Text>
        {item.value3 === value3 && (
          <AntDesign
            style={styles.icon}
            color="black"
            name="Safety"
            size={20}
          />
        )}
      </View>
    );
  };


  const OnSubmit = async () => {
    const Token = await AsyncStorage.getItem('token')

    if (value1 == "") {
      Toast.show("Please select item name!", Toast.BOTTOM);
      return;
    }
    if (value2 == "") {
      Toast.show("Please select handover person name !", Toast.BOTTOM);
      return;
    }
    if (staffID == "") {
      Toast.show("Please enter staff id!", Toast.BOTTOM);
      return;
    }
    if (name == "") {
      Toast.show("Please enter name!", Toast.BOTTOM);
      return;
    }
    if (contactno == "") {
      Toast.show("Please enter contact number!", Toast.BOTTOM);

      return;
    }
    if (email == "") {
      Toast.show("Please enter email address!", Toast.BOTTOM);
      return;
    }
    if (hadnoverbyname == "") {
      Toast.show("Please enter handover by name!", Toast.BOTTOM);
      return;
    }
    if (hadnoverbyadress == "") {
      Toast.show("Please enter handover by address!", Toast.BOTTOM);
      return;
    }
    setShowLoader(true);
    await fetch(API_BASE_URL + 'user/save/handovered', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },
      body: JSON.stringify({
        item_id: value1,
        handover_to: value2,
        passenger_name: name,
        passenger_phone: contactno,
        passenger_email: email,
        handover_by_staff_id: staffID,
        handover_by_name: hadnoverbyname,
        handover_at: hadnoverbyadress,
        remark: remark,
      }),
    })
      .then(response => response.json())
      .then(res => {
        setShowLoader(false);
        if (res?.success === true) {
          if (value2 !== 1) {
            toggleModal()
          }
          setSignatureID(res?.data?.id)
          if (value2 == 1) {
            props.navigation.navigate('DashBoard')
          }
          Toast.show(res?.message, Toast.BOTTOM);

        } else if (res?.success === false) {
          Toast.show(res?.message, Toast.BOTTOM);

          setShowLoader(false);
        }
      })
      .catch(error => {
        Toast.show(error?.message, Toast.BOTTOM);
        setShowLoader(false);
      });
  };


  const OnSubmitSignature = async () => {
    const Token = await AsyncStorage.getItem('token')
    const sign = await AsyncStorage.getItem('sign')
    setShowLoader(true);
    await fetch(API_BASE_URL + 'user/save/signature', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,
      },

      body: JSON.stringify({
        handover_id: signatureID,
        signature: sign,

      }),
    })
      .then(response => response.json())
      .then(res => {
        setShowLoader(false);
        if (res?.success === true) {
          props.navigation.navigate('DashBoard')
          Toast.show(res?.message, Toast.BOTTOM);

        } else if (res?.success === false) {
          Toast.show(res?.message, Toast.BOTTOM);
          setShowLoader(false);
        }
      })
      .catch(error => {
        Toast.show(error?.message, Toast.BOTTOM);
        setShowLoader(false);
      });
  };

  React.useEffect(() => {
    const focusHandler = props.navigation.addListener('focus', () => {
      itemlist()
      handovertolists()
      stafflistss()
      sutoselitem()

    });
    return focusHandler;
  }, [props]);

  const itemlist = async () => {
    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/unhandover/item/list', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },

    })
      .then(response => response.json())
      .then(res => {
        setItemliststore(res?.data)

      })
      .catch(error => {
        Toast.show(error?.message, Toast.BOTTOM);
        setShowLoader(false);
      });
  };



  const handovertolists = async () => {
    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/handover_person/type', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },

    })
      .then(response => response.json())
      .then(res => {
        setHandovertolist(res?.data)

      })
      .catch(error => {
        Toast.show(error?.message, Toast.BOTTOM);
        setShowLoader(false);
      });
  };

  const stafflistss = async () => {
    const Token = await AsyncStorage.getItem('token')
    await fetch(API_BASE_URL + 'user/get/staff/list', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Token}`,

      },

    })
      .then(response => response.json())
      .then(res => {
        setStafflistsss(res?.data)

      })
      .catch(error => {
        Toast.show(error?.message, Toast.BOTTOM);
        setShowLoader(false);
      });
  };


  const itemlistsss = itemliststore?.map(item => ({
    label: `[${item?.id}]` + ' ' + '' + item?.item_name + '' + ' ' + `[${item?.flight_no}]`,
    value: item?.id
  }));



  const handto = handovertolist?.map(item => ({
    label: item?.name,
    value: item?.id
  }));
  const staflists = stafflistsss?.map(item => ({
    label: item?.name,
    value: item?.id
  }));

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => {
      setOpenKey(1)
    });

    const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setOpenKey(2)
    });

    return () => {
      keyboardDidShowListener.remove();
      keyboardDidHideListener.remove();
    };
  }, []);
  return (

    <>
      <View style={{ flex: 1, marginBottom: Platform.OS == "ios" && openKey == 1 ? 320 : null }}>
        <View style={styles.headerroot}>
          <Text style={styles.headertext}>Handover an Item</Text>
        </View>

        <ScrollView scrollEnabled={scrollEnabled}>
          <View style={{
            width: '95%', height: 130, backgroundColor: "#fff", marginTop: 10, alignSelf: "center", elevation: 4, borderRadius: 8, shadowOffset: { width: 1, height: 1 },
            shadowColor: '#566573',
            shadowOpacity: 1.0,

          }}>
            <View>
              <Text style={{ fontSize: 16, color: "#000", fontWeight: "bold", marginLeft: 22, marginTop: 10 }}>Select Item Name</Text>
            </View>
            <View style={{ width: '100%', height: 1, backgroundColor: colors?.blue, marginTop: 12, opacity: 0.4 }}>

            </View>

            <Dropdown
              style={styles.dropdown}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              iconStyle={styles.iconStyle}
              containerStyle={{ backgroundColor: "#EBEDEF" }}
              data={itemlistsss}
              search
              maxHeight={300}
              labelField="label"
              valueField="value"
              placeholder="Select item name"
              searchPlaceholder="Search..."
              value={value1}
              onChange={item => {
                setValue1(item.value);
              }}
              renderItem={renderItem1}
            />
          </View>


          <View style={styles.handovertoroot}>
            <Text style={styles.handover2text}>Handover to</Text>
          </View>

          {/* HANDOVER TO LIST */}

          <View style={{
            width: '95%', height: 310, backgroundColor: "#fff", marginTop: 10, alignSelf: "center", elevation: 4, borderRadius: 8, marginBottom: 10, shadowOffset: { width: 1, height: 1 },
            shadowColor: '#566573',
            shadowOpacity: 1.0,
          }}>
            <View>
              <Text style={{ fontSize: 16, color: "#000", fontWeight: "bold", marginLeft: 22, marginTop: 10 }}>Handover details</Text>
            </View>
            <View style={{ width: '100%', height: 1, backgroundColor: colors?.blue, marginTop: 12, opacity: 0.4 }}>
            </View>

            <Dropdown
              style={styles.dropdown2}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              iconStyle={styles.iconStyle}
              containerStyle={{ backgroundColor: "#EBEDEF" }}
              data={handto}
              search
              maxHeight={300}
              labelField="label"
              valueField="value"
              placeholder="Handover to"
              searchPlaceholder="Search..."
              value={value2}
              onChange={item => {
                setValue2(item.value);
              }}
              renderItem={renderItem2}
            />

            <View>
              <TextInput
                value={name}
                style={styles.nameinput}
                inputStyle={styles.inputStyle}
                labelStyle={styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                label="Name"
                placeholder="Enter Your Name"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setName(text);
                }}
              />
            </View>
            <View>

              <TextInput
                value={contactno}
                style={styles.phoneinput}
                inputStyle={styles.inputStyle}
                labelStyle={styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                label="Contact Number"
                keyboardType="number-pad"
                placeholder="Enter Contact Number"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setContactno(text);
                }}
              />
            </View>

            {
              value2 == "2" ?

                <View>
                  <TextInput
                    value={email}
                    style={styles.phoneinput}
                    inputStyle={styles.inputStyle}
                    labelStyle={styles.labelStyle}
                    placeholderStyle={styles.placeholderStyle}
                    textErrorStyle={styles.textErrorStyle}
                    label="Contact Email"
                    keyboardType='email-address'
                    placeholder=" Enter Contact Email"
                    placeholderTextColor="gray"
                    onChangeText={text => {
                      setEmail(text);
                    }}
                  />
                </View>
                : value2 == "1" ?
                  <View>
                    <TextInput
                      value={email}
                      style={styles.phoneinput}
                      inputStyle={styles.inputStyle}
                      labelStyle={styles.labelStyle}
                      placeholderStyle={styles.placeholderStyle}
                      textErrorStyle={styles.textErrorStyle}
                      label="Staff ID"
                      placeholder=" Enter Staff ID"
                      placeholderTextColor="gray"
                      onChangeText={text => {
                        setEmail(text);
                      }}
                    />
                  </View>
                  : value2 == "3" ?
                    <View>
                      <TextInput
                        value={email}
                        style={styles.phoneinput}
                        inputStyle={styles.inputStyle}
                        labelStyle={styles.labelStyle}
                        placeholderStyle={styles.placeholderStyle}
                        textErrorStyle={styles.textErrorStyle}
                        label="Staff ID"
                        placeholder=" Enter Staff ID"
                        placeholderTextColor="gray"
                        onChangeText={text => {
                          setEmail(text);
                        }}
                      />
                    </View>
                    :
                    null
            }
          </View>




          <View style={styles.handovertoroot}>
            <Text style={styles.handover2text}>Handedover By</Text>
          </View>
          <View style={{
            width: '95%', height: 320, backgroundColor: "#fff", marginTop: 10, alignSelf: "center", elevation: 4, borderRadius: 8, marginBottom: 15, shadowOffset: { width: 1, height: 1 },
            shadowColor: '#566573',
            shadowOpacity: 1.0,
          }}>
            <View>
              <TextInput
                value={hadnoverbyname}
                style={styles.emaiinput}
                inputStyle={styles.inputStyle}
                labelStyle={styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                label="Staff Name"
                placeholder="Enter Staff Name"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setHandoverbyname(text);
                }}
              />
            </View>
            <View>
              <TextInput
                value={staffID}
                style={styles.emaiinput}
                autoCapitalize={"characters"}
                inputStyle={styles.inputStyle}
                labelStyle={styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                label="Staff ID"
                placeholder="Enter Staff ID"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setStaffID(text);
                }}
              />
            </View>

            <View style={{ marginTop: 15 }}>
              <TextInput
                value={hadnoverbyadress}
                style={styles.handoveraddress}
                inputStyle={styles.inputStyle}
                labelStyle={Platform.OS == "ios" ? styles.labelStyle1 : styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                multiline={true}
                label="Location"
                placeholder="Enter Location"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setHandoverbyaddress(text);
                }}
              />
            </View>
            {showLoader && <Loader />}
            <View style={{ marginTop: 15 }}>
              <TextInput
                value={remark}
                style={styles.handoveraddress}
                inputStyle={styles.inputStyle}
                labelStyle={Platform.OS == "ios" ? styles.labelStyle1 : styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                textErrorStyle={styles.textErrorStyle}
                multiline={true}
                label="Remarks"
                placeholder="Enter Remarks"
                placeholderTextColor="gray"
                onChangeText={text => {
                  setRemark(text);
                }}
              />
            </View>
          </View>
          {/* modal for logout */}

          <View style={{ flex: 1 }}>
            <Modal isVisible={isModalVisible}>
              <View style={{ width: "100%", height: 420, backgroundColor: '#000', borderRadius: 10 }}>
                <View style={{ width: '100%', height: 60, backgroundColor: '#fff', justifyContent: 'center' }}>
                  <Text style={{ alignSelf: 'center', fontSize: 16, color: "#00ß", fontWeight: 'bold', textAlign: 'center' }}>I can confirm that the mentioned details are correct</Text>
                </View>
                <SignatureScreen
                  onBegin={() => setScrollEnabled(false)}
                  onEnd={() => setScrollEnabled(true)}
                  ref={ref}
                  confirmText={"Submit"}
                  // onEnd={handleEnd}
                  onOK={handleOK}
                  onEmpty={handleEmpty}
                  onClear={handleClear}
                  onGetData={handleData}
                  autoClear={true}
                // descriptionText={text}
                />
              </View>
            </Modal>
          </View>
          {/* modal for logout */}
          {/* HANDOVER TO LIST END */}
          {
            value2 == "1" ?
              <TouchableOpacity onPress={OnSubmit} style={styles.submitbtnroot}>
                <Text style={{ alignSelf: "center", fontSize: 18, color: "#fff", fontWeight: 'bold', letterSpacing: 1 }}>Submit</Text>
              </TouchableOpacity>
              :
              <TouchableOpacity onPress={OnSubmit} style={styles.submitbtnroot}>
                <Text style={{ alignSelf: "center", fontSize: 18, color: "#fff", fontWeight: 'bold', letterSpacing: 1 }}>Next</Text>
              </TouchableOpacity>
          }
          {
            Platform.OS == "android" ? null :
              <View style={{ width: '100%', height: 20 }}>
              </View>
          }

        </ScrollView >
      </View >


    </>

  );
};

export default ItemHandOver;

const styles = StyleSheet.create({
  dropdown: {
    margin: 16,
    height: 50,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    borderWidth: 1,
    borderWidth: 1,
    borderColor: colors?.blue,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,
  },
  dropdown2: {
    margin: 16,
    height: 50,

    backgroundColor: 'white',
    borderRadius: 8,
    padding: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },

    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
    marginTop: 10,
    borderWidth: 1,
    borderColor: colors?.blue,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,
  },
  dropdownhandoverby: {
    margin: 16,
    height: 50,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 12,
    borderWidth: 1,
    borderColor: colors?.blue,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,
  },


  icon: {
    marginRight: 5,
  },
  item: {
    padding: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  placeholderStyle: {
    fontSize: 16,
  },
  selectedTextStyle: {
    fontSize: 16,
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 16,
  },

  headerroot: {
    width: '100%',
    height: Platform.OS == "android" ? 60 : 100,
    backgroundColor: colors?.blue,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    justifyContent: 'center'
  },

  headertext: {
    fontSize: 18,
    alignSelf: "center",
    marginTop: Platform.OS == 'android' ? 10 : 40,
    color: "#fff",
    fontWeight: 'bold',
    letterSpacing: 1
  },
  container: {
    padding: 16,
  },
  nameinput: {
    height: 50,
    paddingHorizontal: 12,
    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    marginTop: -4,
    borderWidth: 1,
    borderColor: colors?.blue,
    width: "92%",
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,

  },
  nameinputhandoverby: {
    height: 50,
    paddingHorizontal: 12,
    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    marginTop: 10,
    borderWidth: 1,
    borderColor: colors?.blue,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,

  },
  phoneinput: {
    height: 50,
    paddingHorizontal: 12,
    width: "92%",

    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    marginTop: 12,
    borderWidth: 1,
    borderColor: colors?.blue,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,

  },
  emaiinput: {
    height: 50,
    width: "92%",

    paddingHorizontal: 12,
    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    marginTop: 12,
    borderWidth: 1,
    borderColor: colors?.blue,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,

  },
  handoveraddress: {
    height: 80,
    paddingHorizontal: 12,
    borderRadius: 8,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    alignSelf: "center",
    borderWidth: 1,
    borderColor: colors?.blue,
    marginTop: -4,
    width: "92%",
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,


  },
  inputStyle: { fontSize: 16, margin: -10, marginLeft: 0 },
  labelStyle: { fontSize: 16, color: colors?.blue, marginTop: -5, fontWeight: 'bold', },
  labelStyle1: { fontSize: 16, color: colors?.blue, marginTop: -5, fontWeight: 'bold', marginBottom: 10 },
  placeholderStyle: { fontSize: 16 },
  textErrorStyle: {
    fontSize: 16
  },
  handovertoroot: {
    width: '100%',
    height: 40,
    backgroundColor: '#D5D8DC',
    elevation: 4,
    justifyContent: "center",
    marginTop: 10,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#566573',
    shadowOpacity: 1.0,
  },
  handover2text: {
    fontSize: 16,
    alignSelf: 'center',
    color: "#000",
    fontWeight: "bold",
    letterSpacing: 1
  },
  submitbtnroot: {
    width: '95%',
    height: 50,
    backgroundColor: 'green',
    justifyContent: "center",
    borderRadius: 8,
    alignSelf: 'center',
    marginBottom: 60
  }


});
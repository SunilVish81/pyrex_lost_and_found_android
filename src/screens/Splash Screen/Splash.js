import React, { useEffect } from 'react';
import { View, Text, SafeAreaView, Image, StyleSheet, StatusBar, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';
import colors from '../../components/colors';
import AsyncStorage from '@react-native-community/async-storage';
import { Colors } from 'react-native/Libraries/NewAppScreen';


const Splash = (props) => {



    setTimeout(async () => {
        const JWT = await AsyncStorage.getItem('token');
        if (JWT == null) {
            props.navigation.navigate('Login')
            return;
        }
        if (JWT !== null) {
            props.navigation.navigate('TabNavigations')
            return;
        }

    }, 3000);

    return (
        <>
            <StatusBar barStyle="dark-content" backgroundColor={colors?.blue} />
            <View style={{ justifyContent: "center", flex: 1 }}>
                <ImageBackground
                    style={styles.image}
                    source={require('../../assets/Splash.jpeg')}>
                        <View style={{flex:1, justifyContent:'center', marginTop:280}}>
                        <Text
                        animation="slideOutUp"
                        style={styles.txt}
                        delay={1000}
                    >
                        Pyrex Lost & Found
                    </Text>
                        </View>
                
                </ImageBackground>
            </View>
        </>
        // <SafeAreaView style={styles.mainContainer}>
        //     <Animatable.View
        //         animation="fadeInDownBig"
        //         style={styles.splashLogo}
        //         delay={500}

        //     >
        //         <Image
        //             style={styles.mockStyle}
        //             resizeMode="contain"
        //             source={require('../../assets/BrandLogo.png')}
        //         />
        //     </Animatable.View>
        //     <Text
        //         animation="slideOutUp"
        //         style={styles.txt}
        //         delay={1000}
        //     >
        //         Pyrex Lost & Found
        //     </Text>
        // </SafeAreaView>
    );
};



export default Splash;

const styles = StyleSheet.create({

    mainContainer: {
        flex: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
    },
    splashLogo: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50,
    },
    mockStyle: {
        height: 100,
        width: 100,
    },
    txt: {
        fontSize: 26,
        marginTop: 60,
        fontWeight: 'bold',
        color: '#fff',
        alignSelf: 'center',
        
    },
    image: {
        width: '100%',
        height: '100%',
        position: "absolute",
    },
})
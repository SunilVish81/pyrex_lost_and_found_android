import React, { useState } from 'react'
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, TextInput, ActivityIndicator, Platform } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import colors from '../../components/colors';
import { Loader } from '../../components/Loader';
import { API_BASE_URL } from '../../ApiBaseSetUp';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import axios from 'axios';



const Login = (props) => {

    const [emailmob, setEmailmob] = useState('')
    const [password, setPassword] = useState('')
    const [showPassword, setShowPassword] = useState(false);
    const [showLoader, setShowLoader] = useState(false);

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    // const OnSubmit = async () => {
    //     if (emailmob === '') {
    //         Toast.show("Please enter your email!", Toast.BOTTOM);
    //         return;
    //     }

    //     if (password === '') {
    //         Toast.show("Please enter your password!", Toast.BOTTOM);
    //         return;
    //     }

    //     setShowLoader(true);
    //     await fetch(API_BASE_URL + 'user/login', {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json',
    //         },
    //         body: JSON.stringify({
    //             email: emailmob,
    //             password: password,
    //         }),
    //     })
    //         .then(response => response.json())
    //         .then(res => {
    //             setShowLoader(false);
    //             if (res?.success === true) {

    //                 props.navigation.navigate('TabNavigations')
    //                 AsyncStorage.setItem('name', res?.data?.user?.name)
    //                 AsyncStorage.setItem('email', res?.data?.user?.email)
    //                 AsyncStorage.setItem('token', res?.data?.token)
    //                 AsyncStorage.setItem('role', JSON.stringify(res?.data?.user?.role_id))
    //                 Toast.show(res?.message, Toast.BOTTOM);
    //             } else if (res?.success === false) {
    //                 Toast.show(res?.message, Toast.BOTTOM);
    //                 setShowLoader(false);
    //             }
    //         })
    //         .catch(error => {
    //             Toast.show(error?.message, Toast.BOTTOM);
    //             setShowLoader(false);
    //         });
    // };

    const OnSubmit = async () => {
        if (emailmob === '') {
            Toast.show("Please enter your email!", Toast.BOTTOM);
            return;
        }

        if (password === '') {
            Toast.show("Please enter your password!", Toast.BOTTOM);
            return;
        }

        setShowLoader(true);

        try {
            const response = await axios.post(API_BASE_URL + 'user/login', {
                email: emailmob,
                password: password,
            });

            setShowLoader(false);
            console.log(">>>>", response);

            if (response.data.success === true) {
                props.navigation.navigate('TabNavigations');
                AsyncStorage.setItem('name', response.data.data.user.name);
                AsyncStorage.setItem('email', response.data.data.user.email);
                AsyncStorage.setItem('token', response.data.data.token);
                AsyncStorage.setItem('role', JSON.stringify(response.data.data.user.role_id));
                Toast.show(response.data.message, Toast.BOTTOM);
            } else {
                // Check if response.data.message is available, else show a generic error message
                const errorMessage = response.data.message || "An error occurred while processing your request.";
                Toast.show(errorMessage, Toast.BOTTOM);
            }

        } catch (error) {
            Toast.show('These credentials do not match our records.', Toast.BOTTOM);
        } finally {
            setShowLoader(false);
        }
    };


    return (
        <View style={{ flex: 1, }}>
            {showLoader && <Loader />}

            <ScrollView>
                <View>
                    <Image
                        style={styles.brandimg}
                        source={require('../../assets/BrandLogo.png')}
                    />
                </View>
                <View style={{ marginTop: 30 }}>
                    <Text style={styles.welcomebacktext}>Welcome Back</Text>
                    <Text style={styles.logintocontinuetext}>Login to Continue</Text>
                </View>

                <View>

                    <TextInput
                        style={styles.textinput}
                        onChangeText={setEmailmob}
                        value={emailmob}
                        autoCapitalize="none"
                        keyboardType="email-address"
                        placeholder="Enter Email address"
                    />

                </View>


                <View style={styles.textroot}>
                    <TextInput
                        style={styles.textinputpassword}
                        onChangeText={setPassword}
                        value={password}
                        autoCapitalize="none"
                        secureTextEntry={showPassword ? false : true}
                        placeholder="Password"
                    />
                    <View style={styles.eye}>
                        <TouchableOpacity onPress={togglePasswordVisibility}>
                            <Ionicons
                                name={showPassword ? 'eye' : 'eye-off'}
                                size={24}
                                color="blue"
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <TouchableOpacity onPress={OnSubmit}>
                    <View style={styles.btnbackground}>
                        <Text style={
                            styles.next
                        }>Log In</Text>
                    </View>
                </TouchableOpacity>

                <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 10 }}>
                    <Text>Don't have an account create here ?  </Text>
                    <TouchableOpacity onPress={() => props.navigation.navigate('CreateAccount')}>
                        <Text style={{ fontWeight: 'bold' }}>Click here</Text>
                    </TouchableOpacity>
                </View>

            </ScrollView>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    arrowback: {
        resizeMode: 'contain',
        width: 40,
        height: 40,
        tintColor: colors?.blue,
        margin: 10
    },
    brandimg: {
        resizeMode: 'contain',
        width: 120,
        height: 120,
        alignSelf: 'center',
        marginTop: Platform.OS == "android" ? 40 : 60

    },

    textinput: {
        width: "90%",
        height: 45,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius: 8,
        fontSize: 18,
        marginTop: 40,
        alignSelf: "center"

    },
    eye: {
        alignSelf: "flex-end",
        justifyContent: 'center',
        alignSelf: 'center', marginRight: 10
    },

    textinputpassword: {
        height: 45,
        margin: 12,
        borderRadius: 16,
        fontSize: 18,
        alignSelf: 'center'

    },
    next: {
        fontSize: 16,
        color: colors?.white,
        fontWeight: "bold",
        alignSelf: "center",

    },
    btnbackground: {
        width: '90%',
        height: 45,
        backgroundColor: colors?.blue,
        alignSelf: "center",
        borderRadius: 8,
        marginTop: 40,
        justifyContent: "center"
    },
    fp: {

        alignSelf: 'center',
        marginTop: 20,
        color: colors?.blue

    },
    logintocontinuetext: {
        alignSelf: 'center',
        fontSize: 25,
        letterSpacing: 2,
        fontWeight: '500',
        color: '#000',
        letterSpacing: 2,
        marginTop: 12
    },
    welcomebacktext: {
        alignSelf: 'center',
        fontSize: 25,
        letterSpacing: 2
    },
    textroot: {
        borderWidth: 1,
        height: 45,
        width: '90%',
        alignSelf: 'center',
        borderRadius: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20
    }



})

// import React, { useState , useRef } from 'react'
// import SignatureScreen from "react-native-signature-canvas";

// const Login = ({ text, onOK }) => {
//   const ref = useRef();

//   const handleOK = (signature) => {
//     console.log("hello",signature);
//   };
//   const handleEmpty = () => {
//     console.log("Empty");
//   };

//   const handleClear = () => {
//     console.log("clear success!");
//   };

//    const handleData = (data) => {
//     console.log(data);
//   };

//   return (
//     <SignatureScreen
//       ref={ref}
//       // onEnd={handleEnd}
//       onOK={handleOK}
//       onEmpty={handleEmpty}
//       onClear={handleClear}
//       onGetData={handleData}
//       autoClear={true}
//       descriptionText={text}
//     />
//   );
// };

// export default Login;
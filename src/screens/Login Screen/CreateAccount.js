import { StyleSheet, Text, View, useAnimatedValue, TouchableOpacity, ScrollView, Platform } from 'react-native'
import React, { useState, useEffect } from 'react'
import { TextInput } from 'react-native-element-textinput';
import { Dropdown } from 'react-native-element-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons'; // Use appropriate icon library
import AsyncStorage from '@react-native-community/async-storage';

import Toast from 'react-native-simple-toast';
import { API_BASE_URL } from '../../ApiBaseSetUp';
import colors from '../../components/colors';
import { Loader } from '../../components/Loader';




const CreateAccount = (props) => {
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [phone, setPhone] = useState("")
    const [password, setPassword] = useState("")
    const [confirmpassword, setConfirmpassword] = useState("")
    const [value1, setValue1] = useState("");
    const [showLoader, setShowLoader] = useState(false);
    const [roleuser, setRoleuser] = useState([]);



    const OnSubmit = async () => {

        if (name == "") {
            Toast.show(
                'Please enter name !',
                Toast.BOTTOM,

            );
            return;
        }
        if (email == "") {
            Toast.show(
                'Please enter email !',
                Toast.BOTTOM,

            );
            return;
        }
        if (phone == "") {
            Toast.show(
                'Please enter phone number !',
                Toast.BOTTOM,

            );
            return;
        }
        if (password == "") {
            Toast.show(
                'Please enter password !',
                Toast.BOTTOM,

            );
            return;
        }
        if (confirmpassword == "") {
            Toast.show(
                'Please enter confirm password !',
                Toast.BOTTOM,

            );
            return;
        }
        setShowLoader(true);
        await fetch(API_BASE_URL + 'user/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Bearer ${Token}`,

            },
            body: JSON.stringify({
                name: name,
                email: email,
                phone: phone,
                password: password,
                confirm_password: confirmpassword,
                role_id: "2"
            }),
        })
            .then(response => response.json())
            .then(res => {
                setShowLoader(false);
                if (res?.success === true) {
                    props.navigation.navigate('Login')

                    Toast.show(
                        res?.message,
                        Toast.BOTTOM,

                    );

                } else if (res?.success === false) {
                    Toast.show(
                        res?.message,
                        Toast.BOTTOM,

                    );
                    setShowLoader(false);
                }
            })
            .catch(error => {
                Toast.show(
                    error.message || 'An error occurred',
                    Toast.BOTTOM,

                );
                setShowLoader(false);
            });
    };





    return (
        <View style={{ flex: 1 }}>
            <View style={styles.headerroot}>
                <TouchableOpacity onPress={() => props.navigation.navigate("Users")}>
                    <Ionicons name="arrow-back" size={26} color={"#fff"} style={styles.arrback} />
                </TouchableOpacity>
                <Text style={styles.headertext}>Add User</Text>

            </View>
            <ScrollView>
                <View style={{ marginTop: 10 }}>
                    <TextInput
                        value={name}
                        style={styles.handoveraddress}
                        inputStyle={styles.inputStyle}
                        labelStyle={styles.labelStyle}
                        placeholderStyle={styles.placeholderStyle}
                        textErrorStyle={styles.textErrorStyle}
                        label="Name"
                        placeholder="Enter Your Name"
                        placeholderTextColor="gray"
                        onChangeText={text => {
                            setName(text);
                        }}
                    />
                </View>
                <View style={{ marginTop: 10 }}>
                    <TextInput
                        value={email}
                        style={styles.handoveraddress}
                        inputStyle={styles.inputStyle}
                        labelStyle={styles.labelStyle}
                        placeholderStyle={styles.placeholderStyle}
                        textErrorStyle={styles.textErrorStyle}
                        keyboardType='email-address'
                        label="Email"
                        placeholder="Enter Your Email"
                        placeholderTextColor="gray"
                        onChangeText={text => {
                            setEmail(text);
                        }}
                    />
                </View>
                <View style={{ marginTop: 10 }}>
                    <TextInput
                        value={phone}
                        style={styles.handoveraddress}
                        inputStyle={styles.inputStyle}
                        labelStyle={styles.labelStyle}
                        placeholderStyle={styles.placeholderStyle}
                        textErrorStyle={styles.textErrorStyle}
                        label="Phone"
                        keyboardType="number-pad"
                        placeholder="Enter Phone"
                        placeholderTextColor="gray"
                        onChangeText={text => {
                            setPhone(text);
                        }}
                    />
                </View>
                <View style={{ marginTop: 10 }}>
                    <TextInput
                        value={password}
                        style={styles.handoveraddress}
                        inputStyle={styles.inputStyle}
                        labelStyle={styles.labelStyle}
                        placeholderStyle={styles.placeholderStyle}
                        textErrorStyle={styles.textErrorStyle}
                        label="Password"
                        placeholder="Enter Password"
                        placeholderTextColor="gray"
                        onChangeText={text => {
                            setPassword(text);
                        }}
                    />
                </View>
                {showLoader && <Loader />}

                <View style={{ marginTop: 10 }}>
                    <TextInput
                        value={confirmpassword}
                        style={styles.handoveraddress}
                        inputStyle={styles.inputStyle}
                        labelStyle={styles.labelStyle}
                        placeholderStyle={styles.placeholderStyle}
                        textErrorStyle={styles.textErrorStyle}
                        secureTextEntry={true}
                        label="Confirm Password"
                        placeholder=" Confirm Password"
                        placeholderTextColor="gray"
                        onChangeText={text => {
                            setConfirmpassword(text);
                        }}
                    />
                </View>
           
            </ScrollView>
            <View>
                <TouchableOpacity onPress={OnSubmit} style={styles.submitbtnroot}>
                    <Text style={{ alignSelf: "center", fontSize: 18, color: "#fff", fontWeight: 'bold', letterSpacing: 1 }}>Submit</Text>
                </TouchableOpacity>
                {
                    Platform.OS == "android" ? null :
                        <View style={{ width: "100%", height: 20 }}>
                        </View>
                }

            </View>
        </View>
    )
}

export default CreateAccount

const styles = StyleSheet.create({
    handoveraddress: {
        height: 50,
        paddingHorizontal: 12,
        borderRadius: 8,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        alignSelf: "center",
        borderWidth: 1,
        borderColor: colors?.blue,
        width: "92%",


    },
    inputStyle: { fontSize: 16, margin: -10, marginLeft: 0 },
    labelStyle: { fontSize: 16, color: colors?.blue, marginTop: -5, fontWeight: 'bold', },
    placeholderStyle: { fontSize: 16 },
    textErrorStyle: {
        fontSize: 16
    },
    dropdown: {
        margin: 16,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 8,
        padding: 12,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        borderWidth: 1,
        borderWidth: 1,
        borderColor: colors?.blue,
    },

    dropdownhandoverby: {
        margin: 16,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 8,
        padding: 12,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        marginTop: 12,
        borderWidth: 1,
        borderColor: colors?.blue,
    },


    icon: {
        marginRight: 5,
    },
    item: {
        padding: 17,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textItem: {
        flex: 1,
        fontSize: 16,
    },
    placeholderStyle: {
        fontSize: 16,
    },
    selectedTextStyle: {
        fontSize: 16,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    },

    headerroot: {
        width: '100%',
        height: 60,
        backgroundColor: colors?.blue,
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        justifyContent: 'center'
    },

    headertext: {
        fontSize: 18,
        alignSelf: "center",
        marginTop: 10,
        color: "#fff",
        fontWeight: 'bold',
        letterSpacing: 1
    },
    container: {
        padding: 16,
    },
    nameinput: {
        height: 50,
        paddingHorizontal: 12,
        borderRadius: 8,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        alignSelf: "center",
        marginTop: -4,
        borderWidth: 1,
        borderColor: colors?.blue,
        width: "92%"

    },
    headerroot: {
        width: '100%',
        height: Platform.OS == "android" ? 60 : 100,
        backgroundColor: colors?.blue,
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        flexDirection: 'row',
    },

    headertext: {
        fontSize: 18,
        alignSelf: "center",
        color: "#fff",
        fontWeight: 'bold',
        letterSpacing: 1,
        marginLeft: "10%",
        marginTop: Platform.OS == 'android' ? 10 : 40,

        // marginLeft: '20%'
    },
    arrback: {
        alignSelf: "center",
        marginLeft: 10,
        marginTop: Platform.OS == 'android' ? 18 : 58,

    },
    submitbtnroot: {
        width: '95%',
        height: 50,
        backgroundColor: 'green',
        justifyContent: "center",
        borderRadius: 8,
        alignSelf: 'center',
        marginBottom: 5
    }
})
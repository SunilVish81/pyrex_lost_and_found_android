import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from '../screens/Login Screen/Login';
import Splash from '../screens/Splash Screen/Splash';
import TabNavigations from './TabNavigations';
import ItemEdits from '../screens/Tab Screens/Management/ItemEdits';
import ItemDetails from '../screens/Tab Screens/Items/ItemDetails';
import AddUser from '../screens/Tab Screens/Management/Users/AddUser';
import UpdateUser from '../screens/Tab Screens/Management/Users/UpdateUser';
import HandOverDetails from '../screens/Tab Screens/Management/HandOverDetails';
import CreateAccount from '../screens/Login Screen/CreateAccount';


const Stack = createNativeStackNavigator();

const Route = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName='Splash'  >
                <Stack.Screen
                    name="Login"
                    component={Login}
                />
                <Stack.Screen name="TabNavigations" component={TabNavigations} />
                <Stack.Screen name="ItemEdits" component={ItemEdits} />
                <Stack.Screen name="Splash" component={Splash} />
                <Stack.Screen name="ItemDetails" component={ItemDetails} />
                <Stack.Screen name="AddUser" component={AddUser} />
                <Stack.Screen name="UpdateUser" component={UpdateUser} />
                <Stack.Screen name="HandOverDetails" component={HandOverDetails} />
                <Stack.Screen name="CreateAccount" component={CreateAccount} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default Route;
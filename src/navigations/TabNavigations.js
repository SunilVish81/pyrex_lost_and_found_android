import React, { useState, useEffect } from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { View, Text, Image, Platform } from 'react-native';
import colors from "../components/colors";
import DashBoard from "../screens/Tab Screens/DashBoard";
import ItemHandOver from "../screens/Tab Screens/ItemHandOver";
import Management from "../screens/Tab Screens/Management/Management";
import UploadItem from "../screens/Tab Screens/UploadItem";
import Item from "../screens/Tab Screens/Items/Item";
import AsyncStorage from "@react-native-community/async-storage";
import NoAccess from "../screens/Tab Screens/Management/NoAccess";



const Tab = createBottomTabNavigator();

const TabNavigations = (props) => {
    const [roles, setRole] = useState("")
    React.useEffect(() => {
        const focusHandler = props.navigation.addListener('focus', () => {
            getrole()
        });
        return focusHandler;
    }, [props]);


    const getrole = async () => {
        const Token = await AsyncStorage.getItem('role')
        setRole(Token)
    }
    return (
        <Tab.Navigator
            screenOptions={{
                tabBarShowLabel: false,
                tabBarHideOnKeyboard: true,
                tabBarStyle: {
                    position: "absolute",
                    buttom: 20,
                    elevation: 1,
                    height: Platform.OS == "android" ? 55 : 80,
                    elevation: 10,
                    shadowOffset: { width: 1, height: 1 },
                    shadowColor: '#566573',
                    shadowOpacity: 1.0

                },
                tabBarIconStyle: {
                    height: 15,
                }
            }}
        >

            <Tab.Screen name="DashBoard" component={DashBoard} options={{
                headerShown: false,
                tabBarIcon: ({ focused }) => (
                    focused ?
                        <View style={{ alignItems: "center", justifyContent: "center", top: 5 }}>
                            <Image source={require('../assets/das.png')} resizeMode="contain" style={{
                                width: 25,
                                height: 25,
                                tintColor: focused ? '#2E4BA7' : '#748c94'
                            }} />
                            <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 10 }}>Dashboard</Text>
                        </View>
                        :
                        <View style={{ alignItems: "center", justifyContent: "center", top: 5 }}>
                            <Image source={require('../assets/dashboard.png')} resizeMode="contain" style={{
                                width: 20,
                                height: 20,
                                tintColor: focused ? '#2E4BA7' : '#748c94'
                            }} />
                            <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 10 }}>Dashboard</Text>
                        </View>

                ),
            }} />


            <Tab.Screen name="Item" component={Item} options={{
                headerShown: false,
                tabBarIcon: ({ focused }) => (
                    focused ?
                        <View style={{ alignItems: "center", justifyContent: "center", top: 5 }}>
                            <Image source={require('../assets/pro.png')} resizeMode="contain" style={{
                                width: 28,
                                height: 28,
                                tintColor: focused ? '#2E4BA7' : '#748c94'
                            }} />
                            <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 10 }}>Items</Text>
                        </View>
                        :
                        <View style={{ alignItems: "center", justifyContent: "center", top: 5 }}>
                            <Image source={require('../assets/product.png')} resizeMode="contain" style={{
                                width: 20,
                                height: 20,
                                tintColor: focused ? '#2E4BA7' : '#748c94'
                            }} />
                            <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 10 }}>Items</Text>
                        </View>

                ),
            }} />

            <Tab.Screen name="UploadItem" component={UploadItem} options={{
                headerShown: false,
                tabBarIcon: ({ focused }) => (
                    focused ?
                        <View style={{ alignItems: "center", justifyContent: "center", }}>
                            <Image source={require('../assets/plus.png')} resizeMode="contain" style={{
                                width: 35,
                                height: 35,

                            }} />
                            <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 13, marginTop: -2 }}>Upload</Text>
                        </View>
                        :
                        <View style={{ alignItems: "center", justifyContent: "center", }}>
                            <Image source={require('../assets/plus2.png')} resizeMode="contain" style={{
                                width: 35,
                                height: 35,
                                tintColor: focused ? '#2E4BA7' : '#748c94'

                            }} />
                            <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 13, marginTop: -2 }}>Upload</Text>
                        </View>

                ),
            }} />
            <Tab.Screen name="ItemHandOver" component={ItemHandOver} options={{
                headerShown: false,
                tabBarIcon: ({ focused }) => (
                    focused ?
                        <View style={{ alignItems: "center", justifyContent: "center", top: 5 }}>
                            <Image source={require('../assets/pr.png')} resizeMode="contain" style={{
                                width: 30,
                                height: 30,
                                tintColor: focused ? '#2E4BA7' : '#748c94'
                            }} />
                            <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 10 }}>Item handover</Text>
                        </View>
                        :
                        <View style={{ alignItems: "center", justifyContent: "center", top: 5 }}>
                            <Image source={require('../assets/brand.png')} resizeMode="contain" style={{
                                width: 20,
                                height: 20,
                                tintColor: focused ? '#2E4BA7' : '#748c94'
                            }} />
                            <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 10 }}>Item handover</Text>
                        </View>

                ),
            }} />

            {
                roles == 1 || roles == 3 ?
                    <Tab.Screen name="Management" component={Management} options={{
                        headerShown: false,
                        tabBarIcon: ({ focused }) => (
                            focused ?
                                <View style={{ alignItems: "center", justifyContent: "center", top: 5 }}>
                                    <Image source={require('../assets/team-management.png')} resizeMode="contain" style={{
                                        width: 28,
                                        height: 28,
                                        tintColor: focused ? '#2E4BA7' : '#748c94'
                                    }} />
                                    <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 10 }}>Management</Text>
                                </View>
                                :
                                <View style={{ alignItems: "center", justifyContent: "center", top: 5 }}>
                                    <Image source={require('../assets/management.png')} resizeMode="contain" style={{
                                        width: 20,
                                        height: 20,
                                        tintColor: focused ? '#2E4BA7' : '#748c94'
                                    }} />
                                    <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 10 }}>Management</Text>
                                </View>

                        ),
                    }}

                    />
                    :
                    <Tab.Screen name="NoAccess" component={NoAccess} options={{
                        headerShown: false,
                        tabBarIcon: ({ focused }) => (
                            focused ?
                                <View style={{ alignItems: "center", justifyContent: "center", top: 5 }}>
                                    <Image source={require('../assets/team-management.png')} resizeMode="contain" style={{
                                        width: 28,
                                        height: 28,
                                        tintColor: focused ? '#2E4BA7' : '#748c94'
                                    }} />
                                    <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 10 }}>Management</Text>
                                </View>
                                :
                                <View style={{ alignItems: "center", justifyContent: "center", top: 5 }}>
                                    <Image source={require('../assets/management.png')} resizeMode="contain" style={{
                                        width: 20,
                                        height: 20,
                                        tintColor: focused ? '#2E4BA7' : '#748c94'
                                    }} />
                                    <Text style={{ color: focused ? colors?.blue : '#748c94', fontSize: 10 }}>Management</Text>
                                </View>

                        ),
                    }} />
            }




        </Tab.Navigator>
    );
};

export default TabNavigations;


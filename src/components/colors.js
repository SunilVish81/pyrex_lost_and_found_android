const colors = {

  brown: '#696969',
  white: "#fff",
  blue: "#2E4BA7",
  dotcolor: "#E5E5E5",
  black: '#000',
  orange: '#F5B041',
  darkblue: '#2413EC',
  red: '#FE1733'

}

export default colors
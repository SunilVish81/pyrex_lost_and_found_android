import React from "react";
import { View, ActivityIndicator } from "react-native";
import colors from "./colors";


const Loader = props => {
    const { isIndicator } = props;
    return (
        <View
        style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
            //  marginTop: -100
        }}>
        <View
            style={{
                height: 70,
                width: 70,
                // backgroundColor: '#EBF5FB',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10
            }}>
            <ActivityIndicator color={colors?.blue} size={'large'} />
        </View>
    </View>
    )
}

export { Loader };